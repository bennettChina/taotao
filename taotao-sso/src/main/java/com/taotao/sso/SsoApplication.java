package com.taotao.sso;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * 单点登录系统启动类
 *
 * @author 召
 */

@MapperScan("com.taotao.manager.mapper")
@SpringBootApplication
public class SsoApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(SsoApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(SsoApplication.class, args);
    }
}
