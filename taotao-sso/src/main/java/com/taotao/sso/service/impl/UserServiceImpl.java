package com.taotao.sso.service.impl;

import com.taotao.common.pojo.SystemConstants;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.CookieUtils;
import com.taotao.common.utils.ExceptionUtil;
import com.taotao.common.utils.JsonUtils;
import com.taotao.common.utils.StringUtils;
import com.taotao.manager.mapper.TbUserMapper;
import com.taotao.manager.pojo.TbUser;
import com.taotao.manager.pojo.TbUserExample;
import com.taotao.sso.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 处理用户注册、登录相关业务逻辑实现
 *
 * @author 召
 */

@Slf4j
@Service
@PropertySource("classpath:/resource.properties")
public class UserServiceImpl implements UserService {

    @Autowired
    private TbUserMapper tbUserMapper;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Value("${taotao.redis.login.token.prefix}")
    private String userTokenRedisKeyPrefix;
    @Value("${taotao.cookie.name.token}")
    private String tokenCookieName;

    /**
     * 校验数据
     *
     * @param param 校验的数据
     * @param type  要校验的字段
     * @return
     */
    @Override
    public TaotaoResult checkData(String param, Integer type) {
        // 初始化结果
        TaotaoResult taotaoResult = null;
        boolean result = false;
        String sType = "";

        //判断要检验的字段，执行相应的检查
        switch (type) {
            case SystemConstants.TAOTAO_REGISTER_CHECK_TYPE_USERNAME:
                result = checkUsername(param);
                sType = "用户名";
                break;
            case SystemConstants.TAOTAO_REGISTER_CHECK_TYPE_PHONE:
                result = checkPhone(param);
                sType = "手机号";
                break;
            case SystemConstants.TAOTAO_REGISTER_CHECK_TYPE_EMAIL:
                result = checkEmail(param);
                sType = "邮箱";
                break;
            default:
                taotaoResult = TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "不支持的检查类型: " + type);
                break;
        }

        // 返回结果
        if (taotaoResult == null) {
            if (result) {
                taotaoResult = TaotaoResult.ok(result);
            } else {
                taotaoResult = TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, sType + "已经存在", result);
            }
        }
        return taotaoResult;
    }

    /**
     * 注册用户
     *
     * @param tbUser 用户的信息
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public TaotaoResult register(TbUser tbUser) {
        //判断用户的数据是否合法
        if (tbUser == null) {
            return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_BAD_REQUEST, "数据不能为空！");
        }

        try {
            // 完善用户信息
            Date date = new Date();
            tbUser.setCreated(date);
            tbUser.setUpdated(date);
            // md5加密密码
            String md5Password = DigestUtils.md5Hex(tbUser.getPassword());
            tbUser.setPassword(md5Password);

            //保存到数据库
            tbUserMapper.insert(tbUser);
            return TaotaoResult.ok();
        } catch (Exception e) {
            log.error("注册失败！", e);
            return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "注册失败！", ExceptionUtil.getStackTrace(e));
        }
    }

    /**
     * 用户登录
     *
     * @param username 用户名
     * @param password 密码
     * @param request
     * @param response
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public TaotaoResult login(String username, String password, HttpServletRequest request, HttpServletResponse response) {
        // 判断登录信息是否合法
        boolean isBlank = StringUtils.isBlank(username) || StringUtils.isBlank(password);
        if (isBlank) {
            return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_BAD_REQUEST, "用户名或密码不能为空！");
        }

        // 查询数据库
        try {
            // 加密密码
            String md5Password = DigestUtils.md5Hex(password);
            TbUserExample condition = new TbUserExample();
            condition.createCriteria().andUsernameEqualTo(username).andPasswordEqualTo(md5Password);
            List<TbUser> userList = tbUserMapper.selectByExample(condition);

            // 判断是否登录成功
            if (userList == null || userList.size() < 1) {
                return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "用户名或密码错误！");
            }

            //登录成功，生成用户的token（也可以使用jwt工具生成token，安全性更高）
            String token = UUID.randomUUID().toString();

            // 生成redisKey
            String redisKey = getTokenRedisKey(token);

            // 在存入redis之前，置空password，提高安全系数
            TbUser user = userList.get(0);
            user.setPassword(null);

            //将用户信息存入redis,并设置token过期时间为30分钟
            redisTemplate.opsForValue().set(redisKey, JsonUtils.objectToJson(user), 30, TimeUnit.MINUTES);

            // 将token写入cookie中
            int timeout = (int) TimeUnit.MINUTES.toSeconds(30);
            CookieUtils.setCookie(request, response, tokenCookieName, token, timeout);

            // 返回结果
            return TaotaoResult.ok(token);
        } catch (Exception e) {
            log.error("登录失败！", e);
            return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "登录失败", ExceptionUtil.getStackTrace(e));
        }
    }

    /**
     * 根据token获取用户信息
     *
     * @param token 用户登录凭证
     * @return
     */
    @Override
    public TaotaoResult getUserInfoByToken(String token) {
        if (StringUtils.isBlank(token)) {
            return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_BAD_REQUEST, "token不能为空！");
        }

        // 获取redisKey
        String redisKey = getTokenRedisKey(token);

        try {
            // 从redis获取缓存数据
            String userJson = redisTemplate.opsForValue().get(redisKey);
            TbUser tbUser = null;
            if (!StringUtils.isBlank(userJson)) {
                tbUser = JsonUtils.jsonToPojo(userJson, TbUser.class);
            }
            if (tbUser != null) {
                return TaotaoResult.ok(tbUser);
            }
        } catch (Exception e) {
            log.error("获取用户信息失败，token: " + token, e);
        }
        return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "获取用户信息失败！");
    }

    /**
     * 安全退出
     *
     * @param token    用户登录凭证
     * @param request
     * @param response
     * @return
     */
    @Override
    public TaotaoResult logout(String token, HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isBlank(token)) {
            return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_BAD_REQUEST, "token不能为空！");
        }

        // 获取redisKey
        String redisKey = getTokenRedisKey(token);

        try {
            // 从redis删除对应的key
            Boolean isOk = redisTemplate.delete(redisKey);

            // 移除cookie中的数据
            CookieUtils.deleteCookie(request, response, tokenCookieName);

            if (isOk != null && isOk) {
                return TaotaoResult.ok();
            }
        } catch (Exception e) {
            log.error("安全退出失败，token: " + token, e);
        }

        return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "安全退出失败！");
    }

    /**
     * 获取token的redis的key
     *
     * @param token
     * @return
     */
    private String getTokenRedisKey(String token) {
        return userTokenRedisKeyPrefix + token;
    }

    /**
     * 判断用户名是否已存在
     *
     * @param param 用户名
     * @return true|false
     */
    private boolean checkUsername(String param) {
        TbUserExample condition = new TbUserExample();
        condition.createCriteria().andUsernameEqualTo(param);
        List<TbUser> userList = tbUserMapper.selectByExample(condition);
        return userList == null || userList.size() < 1;
    }

    /**
     * 判断手机号是否已存在
     *
     * @param param 手机号
     * @return true|false
     */
    private boolean checkPhone(String param) {
        TbUserExample condition = new TbUserExample();
        condition.createCriteria().andPhoneEqualTo(param);
        List<TbUser> userList = tbUserMapper.selectByExample(condition);
        return userList == null || userList.size() < 1;
    }

    /**
     * 判断邮箱是否已存在
     *
     * @param param 邮箱
     * @return true|false
     */
    private boolean checkEmail(String param) {
        TbUserExample condition = new TbUserExample();
        condition.createCriteria().andEmailEqualTo(param);
        List<TbUser> userList = tbUserMapper.selectByExample(condition);
        return userList == null || userList.size() < 1;
    }
}
