package com.taotao.sso.service;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.manager.pojo.TbUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 处理用户的业务逻辑接口
 *
 * @author 召
 */

public interface UserService {

    /**
     * 校验数据
     *
     * @param param 校验的数据
     * @param type  要校验的字段
     * @return
     */
    TaotaoResult checkData(String param, Integer type);

    /**
     * 注册用户
     *
     * @param tbUser 用户的信息
     * @return
     */
    TaotaoResult register(TbUser tbUser);

    /**
     * 用户登录
     *
     * @param username 用户名
     * @param password 密码
     * @param request
     * @param response
     * @return
     */
    TaotaoResult login(String username, String password, HttpServletRequest request, HttpServletResponse response);

    /**
     * 根据token获取用户信息
     *
     * @param token 用户登录凭证
     * @return
     */
    TaotaoResult getUserInfoByToken(String token);

    /**
     * 安全退出
     *
     * @param token 用户登录凭证
     * @param request
     * @param response
     * @return
     */
    TaotaoResult logout(String token, HttpServletRequest request, HttpServletResponse response);
}
