package com.taotao.sso.web;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.JsonUtils;
import com.taotao.common.utils.StringUtils;
import com.taotao.manager.pojo.TbUser;
import com.taotao.sso.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 召
 */

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 校验数据
     *
     * @param param    校验的数据
     * @param type     要校验的字段
     * @param callback 可选参数：回调函数,如果有此参数表示此方法为jsonp请求，需要支持jsonp。
     * @return
     */
    @CrossOrigin(origins = {"localhost", "127.0.0.1", "www.taotao.com"})
    @GetMapping("/check/{param}/{type}")
    public Object checkData(@PathVariable String param, @PathVariable Integer type, @RequestParam(required = false) String callback) {
        TaotaoResult result = userService.checkData(param, type);
        if (!StringUtils.isBlank(callback)) {
            return callback + "(" + JsonUtils.objectToJson(result) + ");";
        }
        return result;
    }

    /**
     * 注册用户
     *
     * @param tbUser 用户信息
     * @return
     */
    @PostMapping("/register")
    public TaotaoResult register(TbUser tbUser) {
        return userService.register(tbUser);
    }

    /**
     * 用户登录
     *
     * @param username 用户名
     * @param password 密码
     * @param request
     * @param response
     * @return
     */
    @PostMapping("/login")
    public TaotaoResult login(String username, String password, HttpServletRequest request, HttpServletResponse response) {
        return userService.login(username, password, request, response);
    }

    /**
     * 通过token查询用户信息
     *
     * @param token    用户登录凭证
     * @param callback 可选参数：回调函数,如果有此参数表示此方法为jsonp请求，需要支持jsonp。
     * @return
     */
    @CrossOrigin(origins = {"localhost", "127.0.0.1", "www.taotao.com"})
    @GetMapping("/token/{token}")
    public Object getUserInfoByToken(@PathVariable String token, @RequestParam(required = false) String callback) {
        TaotaoResult taotaoResult = userService.getUserInfoByToken(token);
        if (!StringUtils.isBlank(callback)) {
            return callback + "(" + JsonUtils.objectToJson(taotaoResult) + ");";
        }
        return taotaoResult;
    }

    /**
     * 安全退出
     *
     * @param token    用户登录凭证
     * @param callback 可选参数：回调函数,如果有此参数表示此方法为jsonp请求，需要支持jsonp。
     * @return
     */
    @RequestMapping("/logout/{token}")
    public Object logout(@PathVariable String token, HttpServletRequest request, HttpServletResponse response,
                         @RequestParam(required = false) String callback) {
        TaotaoResult taotaoResult = userService.logout(token, request, response);
        if (!StringUtils.isBlank(callback)) {
            return callback + "(" + JsonUtils.objectToJson(taotaoResult) + ");";
        }
        return taotaoResult;
    }

}
