package com.taotao.sso.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 仅处理页面跳转请求
 *
 * @author 召
 */

@Controller
@RequestMapping("/page")
public class PageController {

    @RequestMapping("/{page}")
    public String toPage(@PathVariable("page") String page) {
        return page;
    }

    @RequestMapping("/showLogin")
    public String showLogin(String redirect, Model model) {
        model.addAttribute("redirect", redirect);
        return "login";
    }

    @RequestMapping("/showRegister")
    public String showRegister() {
        return "register";
    }
}
