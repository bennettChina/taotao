package com.taotao.rest.service;

import com.taotao.common.pojo.TaotaoResult;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 测试item_content中的业务逻辑
 *
 * @author 召
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class ItemContentTest {

    @Autowired
    private ItemContentService itemContentService;

    @Test
    public void testGetAllContent() {
        TaotaoResult result = itemContentService.getAllContent(89L);
        if (result.getStatus().equals(TaotaoResult.RESPONSE_CODE_OK)) {
            Assert.assertNotNull("不应该为空！", result.getData());
        }
    }
}
