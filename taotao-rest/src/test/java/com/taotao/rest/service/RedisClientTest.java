package com.taotao.rest.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Set;

/**
 * @author 召
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisClientTest {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Test
    public void testRedis() {
        Set<String> keys = redisTemplate.keys("*");
        log.info("操作前存在的keys: " + keys);

        String key = "lanou_F5";

        redisTemplate.opsForList().rightPushAll(key, "宋超", "国胜", "国伟", "高飞", "凯哥");

        Long size = redisTemplate.opsForList().size(key);
        log.info("当前" + key + "值的数量: " + size);

        List<String> values = redisTemplate.opsForList().range(key, 0, size);
        log.info("当前" + key + "的值: " + values);

        keys = redisTemplate.keys("*");
        log.info("操作后存在的keys: " + keys);
    }
}
