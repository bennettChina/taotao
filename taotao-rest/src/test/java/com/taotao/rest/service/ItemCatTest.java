package com.taotao.rest.service;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.JsonUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 测试itemCat业务
 *
 * @author 召
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class ItemCatTest {

    @Autowired
    private ItemCatService itemCatService;

    @Test
    public void testGetItemCatList() {
        TaotaoResult result = itemCatService.getItemCat();
        String json = JsonUtils.objectToJson(result.getData());
        System.out.println(json);
        Assert.assertNotNull("不应该为空，应有值",result.getData());
    }
}
