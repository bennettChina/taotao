package com.taotao.rest.web;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.rest.service.ItemCatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商品分类相关请求处理
 *
 * @author 召
 */

@RestController
@RequestMapping("/itemcat")
public class ItemCatController {

    @Autowired
    private ItemCatService itemCatService;

    /**
     * 查询所有的分类item
     *
     * @return
     */
    @CrossOrigin
    @RequestMapping("/all")
    public TaotaoResult getItemCatList() {
        return itemCatService.getItemCat();
    }

    /**
     * 清理tb_item_cat表在redis中的缓存
     *
     * @return
     */
    @RequestMapping("/clear")
    public TaotaoResult clearCatCache() {
        return itemCatService.clearCatCache();
    }
}
