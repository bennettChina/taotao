package com.taotao.rest.web;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.rest.service.ItemContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * item_content相关restFul服务请求处理
 *
 * @author 召
 */

@RestController
@RequestMapping("/itemContent")
public class ItemContentController {

    @Autowired
    private ItemContentService itemContentService;

    /**
     * 获取所有的广告内容
     *
     * @return
     */
    @RequestMapping("/all/{category_id}")
    public TaotaoResult getItemContentList(@PathVariable("category_id") Long categoryId) {
        return itemContentService.getAllContent(categoryId);
    }

    /**
     * 清理缓存
     *
     * @param categoryId 分类id
     * @return
     */
    @RequestMapping("/clear/{category_id}")
    public TaotaoResult clearContentCache(@PathVariable("category_id") Long categoryId) {
        return itemContentService.clearContentCache(categoryId);
    }
}
