package com.taotao.rest.web;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.rest.service.TbItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 处理商品相关rest服务请求
 *
 * @author 召
 */

@RestController
@RequestMapping("/item")
public class TbItemController {

    @Autowired
    private TbItemService tbItemService;

    /**
     * 获取商品详细信息
     *
     * @param itemId 商品id
     * @return
     */
    @RequestMapping("/get_item_details/{item_id}")
    public TaotaoResult getItemDetails(@PathVariable("item_id") Long itemId) {
        return tbItemService.getItemDetails(itemId);
    }

    /**
     * 获取商品的描述信息
     *
     * @param itemId 商品id
     * @return
     */
    @RequestMapping("/get_item_desc/{item_id}")
    public TaotaoResult getItemDesc(@PathVariable("item_id") Long itemId) {
        return tbItemService.getItemDesc(itemId);
    }

    /**
     * 获取商品的规格参数
     *
     * @param itemId 商品id
     * @return
     */
    @RequestMapping("/get_item_param/{item_id}")
    public TaotaoResult getItemParam(@PathVariable("item_id") Long itemId) {
        return tbItemService.getItemParam(itemId);
    }
}
