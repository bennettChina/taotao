package com.taotao.rest.web;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.rest.service.ItemCacheClearService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 处理缓存
 *
 * @author 召
 */

@RestController
public class ItemCacheClearController {

    @Autowired
    private ItemCacheClearService itemCacheClearService;

    /**
     * 清除tb_item表在redis中的缓存
     *
     * @param ids 商品id
     * @return
     */
    @RequestMapping("/item/clear")
    public TaotaoResult clearItemCache(@RequestParam("ids") String ids) {
        return itemCacheClearService.clearItemCache(strToLongArray(ids));
    }

    /**
     * 清除tb_item_desc表在redis中的缓存
     *
     * @param ids 商品id
     * @return
     */
    @RequestMapping("/itemDesc/clear")
    public TaotaoResult clearItemDescCache(@RequestParam("ids") String ids) {
        return itemCacheClearService.clearItemDescCache(strToLongArray(ids));
    }

    /**
     * 清除tb_item_param_item表在redis中的缓存
     *
     * @param ids 商品id
     * @return
     */
    @RequestMapping("/itemParamItem/clear")
    public TaotaoResult clearItemParamItemCache(@RequestParam("ids") String ids) {
        return itemCacheClearService.clearItemParamItemCache(strToLongArray(ids));
    }

    /**
     * 字符串转数组
     *
     * @param str 字符串
     * @return long[]
     */
    private Long[] strToLongArray(String str) {
        // 分割参数
        String[] strings = str.split(",");
        // 转换位long数组
        Long[] arr = new Long[strings.length];
        for (int i = 0; i < strings.length; i++) {
            arr[i] = Long.valueOf(strings[i]);
        }
        return arr;
    }
}
