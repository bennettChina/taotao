package com.taotao.rest.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 商品分类实体类（作为返回类型供调用使用）
 *
 * @author 召
 */

@Setter
@Getter
public class ItemCat {

    /**
     * 类目的url
     */
    @JsonProperty("u")
    private String url;

    /**
     * 类目的名字
     */
    @JsonProperty("n")
    private String name;

    /**
     * 类目的所有数据
     */
    @JsonProperty("i")
    private List<?> item;
}
