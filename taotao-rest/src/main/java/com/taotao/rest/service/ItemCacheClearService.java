package com.taotao.rest.service;

import com.taotao.common.pojo.TaotaoResult;

/**
 * 处理缓存的逻辑接口
 *
 * @author 召
 */

public interface ItemCacheClearService {

    /**
     * 清理tb_item表的缓存
     *
     * @param ids 商品id
     * @return
     */
    TaotaoResult clearItemCache(Long[] ids);

    /**
     * 清理tb_item_desc表的缓存
     *
     * @param ids 商品id
     * @return
     */
    TaotaoResult clearItemDescCache(Long[] ids);

    /**
     * 清理tb_item_param_item表的缓存
     *
     * @param ids 商品id
     * @return
     */
    TaotaoResult clearItemParamItemCache(Long[] ids);

}
