package com.taotao.rest.service;

import com.taotao.common.pojo.TaotaoResult;

/**
 * item_content表相关业务逻辑接口
 *
 * @author 召
 */

public interface ItemContentService {

    /**
     * 获取所有的广告内容
     *
     * @param categoryId
     * @return
     */
    TaotaoResult getAllContent(Long categoryId);

    /**
     * 清空redis中对应key的值
     *
     * @param categoryId
     * @return
     */
    TaotaoResult clearContentCache(Long categoryId);
}
