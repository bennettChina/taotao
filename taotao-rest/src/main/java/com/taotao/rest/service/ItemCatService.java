package com.taotao.rest.service;

import com.taotao.common.pojo.TaotaoResult;

/**
 * 商品的分类业务逻辑接口
 *
 * @author 召
 */

public interface ItemCatService {

    /**
     * 获取商品分类item
     *
     * @return
     */
    TaotaoResult getItemCat();

    /**
     * 清空redis中的缓存
     *
     * @return
     */
    TaotaoResult clearCatCache();
}
