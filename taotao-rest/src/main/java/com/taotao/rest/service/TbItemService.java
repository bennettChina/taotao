package com.taotao.rest.service;

import com.taotao.common.pojo.TaotaoResult;

/**
 * 处理商品相关rest服务业务的接口
 *
 * @author 召
 */

public interface TbItemService {

    /**
     * 获取商品的详细信息
     *
     * @param itemId 商品id
     * @return
     */
    TaotaoResult getItemDetails(Long itemId);

    /**
     * 获取商品的描述信息
     *
     * @param itemId 商品id
     * @return
     */
    TaotaoResult getItemDesc(Long itemId);

    /**
     * 获取商品的规格参数
     *
     * @param itemId 商品id
     * @return
     */
    TaotaoResult getItemParam(Long itemId);
}
