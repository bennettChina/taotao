package com.taotao.rest.service.impl;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.ExceptionUtil;
import com.taotao.rest.service.ItemCacheClearService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 处理缓存的逻辑实现
 *
 * @author 召
 */

@Slf4j
@Service
@PropertySource("classpath:/resource.properties")
public class ItemCacheClearServiceImpl implements ItemCacheClearService {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Value("${taotao.redis.keys.item}")
    private String itemKeyPrefix;
    @Value("${taotao.redis.keys.itemDesc}")
    private String itemDescKeyPrefix;
    @Value("${taotao.redis.keys.itemParamItem}")
    private String itemParamItemKeyPrefix;

    /**
     * 清理tb_item表的缓存
     *
     * @param ids 商品id
     * @return 响应状态和信息
     */
    @Override
    public TaotaoResult clearItemCache(Long[] ids) {
        List<String> keyList = new ArrayList<>();
        for (Long id : ids) {
            keyList.add(itemKeyPrefix + id);
        }
        return clearCache(keyList);
    }

    /**
     * 清理tb_item_desc表的缓存
     *
     * @param ids 商品id
     * @return
     */
    @Override
    public TaotaoResult clearItemDescCache(Long[] ids) {
        List<String> keyList = new ArrayList<>();
        for (Long id : ids) {
            keyList.add(itemDescKeyPrefix + id);
        }
        return clearCache(keyList);
    }

    /**
     * 清理tb_item_param_item表的缓存
     *
     * @param ids 商品id
     * @return
     */
    @Override
    public TaotaoResult clearItemParamItemCache(Long[] ids) {
        List<String> keyList = new ArrayList<>();
        for (Long id : ids) {
            keyList.add(itemParamItemKeyPrefix + id);
        }
        return clearCache(keyList);
    }

    /**
     * 清理key对应的缓存
     *
     * @param key
     * @return
     */
    private TaotaoResult clearCache(Collection<String> key) {
        // 清空缓存
        try {
            redisTemplate.delete(key);
        } catch (Exception e) {
            log.error(ExceptionUtil.getStackTrace(e));
        }
        return TaotaoResult.ok();
    }

}
