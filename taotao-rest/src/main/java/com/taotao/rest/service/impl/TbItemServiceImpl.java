package com.taotao.rest.service.impl;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.ExceptionUtil;
import com.taotao.common.utils.JsonUtils;
import com.taotao.common.utils.StringUtils;
import com.taotao.manager.mapper.TbItemDescMapper;
import com.taotao.manager.mapper.TbItemMapper;
import com.taotao.manager.mapper.TbItemParamItemMapper;
import com.taotao.manager.pojo.TbItem;
import com.taotao.manager.pojo.TbItemDesc;
import com.taotao.manager.pojo.TbItemParamItem;
import com.taotao.manager.pojo.TbItemParamItemExample;
import com.taotao.rest.service.TbItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 处理商品相关rest服务的实现类
 *
 * @author 召
 */

@Slf4j
@Service
@PropertySource("classpath:/resource.properties")
public class TbItemServiceImpl implements TbItemService {

    @Autowired
    private TbItemMapper tbItemMapper;
    @Autowired
    private TbItemDescMapper tbItemDescMapper;
    @Autowired
    private TbItemParamItemMapper tbItemParamItemMapper;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Value("${taotao.redis.keys.item}")
    private String itemKeyPrefix;
    @Value("${taotao.redis.keys.itemDesc}")
    private String itemDescKeyPrefix;
    @Value("${taotao.redis.keys.itemParamItem}")
    private String itemParamItemKeyPrefix;

    @Transactional(readOnly = true)
    @Override
    public TaotaoResult getItemDetails(Long itemId) {
        // 从缓存中拿数据
        String key = itemKeyPrefix + itemId;
        try {
            String itemCache = redisTemplate.opsForValue().get(key);
            if (!StringUtils.isBlank(itemCache)) {
                TbItem tbItem = JsonUtils.jsonToPojo(itemCache, TbItem.class);
                return TaotaoResult.ok(tbItem);
            }
        } catch (Exception e) {
            log.error("获取key为: " + key + "的商品信息缓存失败！" + ExceptionUtil.getStackTrace(e));
        }

        // 查询数据库
        TbItem tbItem = tbItemMapper.selectByPrimaryKey(itemId);

        // 处理查询结果为空的情况
        if (tbItem == null) {
            log.warn("没有查到ID为: " + itemId + "的商品信息");
            return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "无该商品信息");
        }

        // 把数据放到缓存中
        try {
            redisTemplate.opsForValue().set(key, JsonUtils.objectToJson(tbItem));
            // 设置过期时间，有效期一天
            redisTemplate.expire(key, 1, TimeUnit.DAYS);
        } catch (Exception e) {
            log.error("存key为: " + key + "的商品信息缓存出现问题了！" + ExceptionUtil.getStackTrace(e));
        }

        // 返回结果
        return TaotaoResult.ok(tbItem);
    }

    @Transactional(readOnly = true)
    @Override
    public TaotaoResult getItemDesc(Long itemId) {
        // 从缓存中拿数据
        String key = itemDescKeyPrefix + itemId;
        try {
            String itemCache = redisTemplate.opsForValue().get(key);
            if (!StringUtils.isBlank(itemCache)) {
                TbItemDesc tbItemDesc = JsonUtils.jsonToPojo(itemCache, TbItemDesc.class);
                return TaotaoResult.ok(tbItemDesc);
            }
        } catch (Exception e) {
            log.error("获取key为: " + key + "的商品描述信息缓存失败！" + ExceptionUtil.getStackTrace(e));
        }

        // 查询数据库
        TbItemDesc tbItemDesc = tbItemDescMapper.selectByPrimaryKey(itemId);

        // 处理查询结果为空的情况
        if (tbItemDesc == null) {
            log.warn("没有查到ID为: " + itemId + "的商品描述信息");
            return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "无商品描述信息");
        }

        // 存到缓存
        try {
            redisTemplate.opsForValue().set(key, JsonUtils.objectToJson(tbItemDesc));
            // 设置过期时间，有效期一天
            redisTemplate.expire(key, 1, TimeUnit.DAYS);
        } catch (Exception e) {
            log.error("存key为" + key + "的商品描述信息缓存出问题了！" + ExceptionUtil.getStackTrace(e));
        }

        // 返回结果
        return TaotaoResult.ok(tbItemDesc);
    }

    @Transactional(readOnly = true)
    @Override
    public TaotaoResult getItemParam(Long itemId) {
        // 从缓存中拿数据
        String key = itemParamItemKeyPrefix + itemId;
        try {
            String itemCache = redisTemplate.opsForValue().get(key);
            if (!StringUtils.isBlank(itemCache)) {
                TbItemParamItem tbItemParamItem = JsonUtils.jsonToPojo(itemCache, TbItemParamItem.class);
                return TaotaoResult.ok(tbItemParamItem);
            }
        } catch (Exception e) {
            log.error("获取key为" + key + "的商品规格参数缓存失败！" + ExceptionUtil.getStackTrace(e));
        }

        // 查询数据库
        // 创建查询条件
        TbItemParamItemExample condition = new TbItemParamItemExample();
        condition.createCriteria().andItemIdEqualTo(itemId);
        // 查询数据
        List<TbItemParamItem> paramItemList = tbItemParamItemMapper.selectByExampleWithBLOBs(condition);
        // 处理空值情况
        if (paramItemList == null || paramItemList.size() < 1) {
            log.warn("没有查到ID为: " + itemId + "的商品规格参数信息");
            return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "没有查到ID为: " + itemId + "的商品规格参数信息");
        }

        // 把数据存入缓存
        try {
            redisTemplate.opsForValue().set(key, JsonUtils.objectToJson(paramItemList.get(0)));
            // 设置过期时间，有效期一天
            redisTemplate.expire(key, 1, TimeUnit.DAYS);
        } catch (Exception e) {
            log.error("存key为" + key + "的商品规格参数缓存出问题了！" + ExceptionUtil.getStackTrace(e));
        }

        // 返回结果
        return TaotaoResult.ok(paramItemList.get(0));
    }
}
