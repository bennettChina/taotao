package com.taotao.rest.service.impl;

import com.taotao.common.pojo.SystemConstants;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.JsonUtils;
import com.taotao.common.utils.StringUtils;
import com.taotao.manager.mapper.TbItemCatMapper;
import com.taotao.manager.pojo.TbItemCat;
import com.taotao.manager.pojo.TbItemCatExample;
import com.taotao.rest.pojo.ItemCat;
import com.taotao.rest.service.ItemCatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品的分类业务逻辑实现类
 *
 * @author 召
 */

@PropertySource("classpath:/resource.properties")
@Service
public class ItemCatServiceImpl implements ItemCatService {

    @Autowired
    private TbItemCatMapper tbItemCatMapper;

    @Autowired
    private StringRedisTemplate redisTemplate;
    @Value("${taotao.redis.keys.hashPrefix}")
    private String hashKeyPrefix;
    @Value("${taotao.redis.keys.itemCat}")
    private String itemCat;

    @Transactional(readOnly = true)
    @Override
    public TaotaoResult getItemCat() {
        // 添加redis缓存处理
        try {
            // 缓存逻辑，判断缓存中是否有内容
            String itemCatStr = (String) redisTemplate.opsForHash().get(hashKeyPrefix, itemCat);
            if (!StringUtils.isBlank(itemCatStr)) {
                // 把json字符串转化为对象列表
                List<ItemCat> tbItemCatList = JsonUtils.jsonToList(itemCatStr, ItemCat.class);
                // 返回结果
                return TaotaoResult.ok(tbItemCatList);
            }
        } catch (Exception e) {
            e.printStackTrace();
            // 缓存不能影响正常逻辑
        }

        // 查询数据库
        // 创建查询条件
        TbItemCatExample condition = new TbItemCatExample();
        condition.createCriteria().andStatusNotEqualTo(SystemConstants.TB_ITEM_CAT_STATUS_DELETED);
        // 执行查询操作
        List<TbItemCat> tbItemCatList = tbItemCatMapper.selectByExample(condition);
        List<Object> itemCatList = loadItemCatWithPro(tbItemCatList, 0L);

        // 把结果放进redis中
        try {
            redisTemplate.opsForHash().put(hashKeyPrefix, itemCat, JsonUtils.objectToJson(itemCatList));
        } catch (Exception e) {
            e.printStackTrace();
            // 缓存不能影响正常逻辑
        }

        // 返回结果
        return TaotaoResult.ok(itemCatList);
    }

    @Transactional(readOnly = true)
    @Override
    public TaotaoResult clearCatCache() {
        // 清空itemCat中的内容
        redisTemplate.opsForHash().delete(hashKeyPrefix, itemCat);
        return TaotaoResult.ok();
    }

    /**
     * 递归查询数据库设置节点内容(大数据量时效率低，但小数据量时效率比递归内存高)
     *
     * @param parentId 父类目id
     * @return
     */
    @Deprecated
    private List<Object> loadItemCat(Long parentId) {
        List<Object> result = new ArrayList<>();

        // 创建查询条件
        TbItemCatExample condition = new TbItemCatExample();
        condition.createCriteria().andParentIdEqualTo(parentId).andStatusNotEqualTo(SystemConstants.TB_ITEM_CAT_STATUS_DELETED);

        // 执行查询操作
        List<TbItemCat> tbItemCatList = tbItemCatMapper.selectByExample(condition);
        int count = 0;
        for (TbItemCat tbItemCat : tbItemCatList) {
            if (tbItemCat.getIsParent()) {
                ItemCat itemCat = new ItemCat();
                itemCat.setUrl("/products/" + tbItemCat.getId());
                // 如果当前节点已经是父节点则如下设置
                if (parentId.equals(0)) {
                    itemCat.setName("<a href='/products/" + tbItemCat.getId() + "'>" + tbItemCat.getName() + "</a>");
                } else {
                    itemCat.setName(tbItemCat.getName());
                }
                // 递归查询剩余的所有的子item
                itemCat.setItem(loadItemCat(tbItemCat.getId()));
                result.add(itemCat);
                // 对商品类目数量做出限制
                count++;
                if (count >= 14) {
                    break;
                }
            } else {
                String itemCat = "/products/" + tbItemCat.getId() + "|" + tbItemCat.getName();
                result.add(itemCat);
            }
        }
        return result;
    }

    /**
     * 递归内存设置节点内容（大数据量时效率比递归查数据库高）
     *
     * @param data     内存数据
     * @param parentId 父节点id
     * @return
     */
    private List<Object> loadItemCatWithPro(List<TbItemCat> data, Long parentId) {
        List<Object> result = new ArrayList<>();
        int count = 0;
        // 设置节点内容
        for (TbItemCat tbItemCat : data) {
            // 保证每次递归的数据是parentId对应的数据，否则会出现“死递归”
            if (tbItemCat.getParentId().equals(parentId)) {
                if (tbItemCat.getIsParent()) {
                    ItemCat itemCat = new ItemCat();
                    itemCat.setUrl("/products/" + tbItemCat.getId());
                    // 如果当前节点已经是父节点则如下设置
                    if (parentId.equals(0)) {
                        itemCat.setName("<a href='/products/" + tbItemCat.getId() + "'>" + tbItemCat.getName() + "</a>");
                    } else {
                        itemCat.setName(tbItemCat.getName());
                    }
                    // 递归查询剩余的所有的子item
                    itemCat.setItem(loadItemCatWithPro(data, tbItemCat.getId()));
                    result.add(itemCat);
                    // 对商品类目数量做出限制
                    count++;
                    if (count >= 14) {
                        break;
                    }
                } else {
                    String itemCat = "/products/" + tbItemCat.getId() + "|" + tbItemCat.getName();
                    result.add(itemCat);
                }
            }
        }

        return result;
    }
}
