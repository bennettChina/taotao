package com.taotao.rest.service.impl;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.ExceptionUtil;
import com.taotao.common.utils.JsonUtils;
import com.taotao.common.utils.StringUtils;
import com.taotao.manager.mapper.TbContentMapper;
import com.taotao.manager.pojo.TbContent;
import com.taotao.manager.pojo.TbContentExample;
import com.taotao.rest.service.ItemContentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 召
 */

@Slf4j
@PropertySource("classpath:/resource.properties")
@Service
public class ItemContentServiceImpl implements ItemContentService {

    @Autowired
    private TbContentMapper tbContentMapper;

    @Autowired
    private StringRedisTemplate redisTemplate;
    @Value("${taotao.redis.keys.hashPrefix}")
    private String hashKeyPrefix;
    @Value("${taotao.redis.keys.contentPrefix}")
    private String contentKeyPrefix;

    @Transactional(readOnly = true)
    @Override
    public TaotaoResult getAllContent(Long categoryId) {
        String key = null;
        try {
            // 添加redis缓存处理
            key = contentKeyPrefix + categoryId;
            // 缓存逻辑，判断缓存中是否有内容
            String contentStr = (String) redisTemplate.opsForHash().get(hashKeyPrefix, key);
            if (!StringUtils.isBlank(contentStr)) {
                // 把json字符串转化为对象列表
                List<TbContent> tbContentList = JsonUtils.jsonToList(contentStr, TbContent.class);
                // 返回结果
                return TaotaoResult.ok(tbContentList);
            }
        } catch (Exception e) {
            log.error(ExceptionUtil.getStackTrace(e));
            // 缓存不能影响正常逻辑
        }

        // 从数据库加载数据
        TbContentExample condition = new TbContentExample();
        condition.createCriteria().andCategoryIdEqualTo(categoryId);
        List<TbContent> tbContentList = tbContentMapper.selectByExampleWithBLOBs(condition);

        // 把结果加到redis中
        try {
            String value = JsonUtils.objectToJson(tbContentList);
            if (!StringUtils.isBlank(value)) {
                redisTemplate.opsForHash().put(hashKeyPrefix, key, value);
            }
        } catch (Exception e) {
            log.error(ExceptionUtil.getStackTrace(e));
            // 缓存不能影响正常逻辑
        }

        // 返回结果
        if (tbContentList != null && tbContentList.size() > 0) {
            return TaotaoResult.ok(tbContentList);
        }
        return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "查询失败或无数据！");
    }

    @Override
    public TaotaoResult clearContentCache(Long categoryId) {
        // 清空categoryId对应的内容
        try {
            redisTemplate.opsForHash().delete(hashKeyPrefix, contentKeyPrefix + categoryId);
        } catch (Exception e) {
            log.error(ExceptionUtil.getStackTrace(e));
        }
        return TaotaoResult.ok();
    }
}
