package com.taotao.common.pojo;

import java.util.List;

/**
 * 响应的json数据格式
 *
 * @author 召
 */
public class EasyUiResult {

    private Integer total;

    private List<?> rows;

    public EasyUiResult(Integer total, List<?> rows) {
        this.total = total;
        this.rows = rows;
    }

    public EasyUiResult(long total, List<?> rows) {
        this.total = (int) total;
        this.rows = rows;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<?> getRows() {
        return rows;
    }

    public void setRows(List<?> rows) {
        this.rows = rows;
    }


}
