package com.taotao.common.pojo;

/**
 * 系统常量类
 *
 * @author 召
 */

public class SystemConstants {

    /**
     * tb_item_cat表中状态：正常
     */
    public static final int TB_ITEM_CAT_STATUS_NORMAL = 1;

    /**
     * tb_item_cat表中状态：删除
     */
    public static final int TB_ITEM_CAT_STATUS_DELETED = 2;

    /**
     * tb_content_category表状态：正常
     */
    public static final int TB_CONTENT_CATEGORY_STATUS_NORMAL = 1;

    /**
     * tb_content_category表状态：删除
     */
    public static final int TB_CONTENT_CATEGORY_STATUS_DELETED = 2;

    /**
     * tb_content_category表最小排序值
     */
    public static final int TB_CONTENT_CATEGORY_SORT = 1;

    /**
     * tb_item表商品状态：正常
     */
    public static final byte TB_ITEM_STATUS_NORMAL = 1;

    /**
     * tb_item表商品状态：下架
     */
    public static final byte TB_ITEM_STATUS_DISCONTINUED = 2;

    /**
     * tb_item表商品状态：删除
     */
    public static final byte TB_ITEM_STATUS_DELETED = 3;

    /**
     * easyui异步树节点状态：开启
     */
    public static final String TREE_NODE_STATUS_OPEN = "open";

    /**
     * easyui异步树节点状态：关闭
     */
    public static final String TREE_NODE_STATUS_CLOSE = "closed";

    /**
     * 图片存储的日期路径
     */
    public static final String DATE_FORMAT = "yyyy/MM/dd";

    /**
     * 用户数据校验：校验用户名
     */
    public static final int TAOTAO_REGISTER_CHECK_TYPE_USERNAME = 1;

    /**
     * 用户数据校验：校验手机号
     */
    public static final int TAOTAO_REGISTER_CHECK_TYPE_PHONE = 2;

    /**
     * 用户数据校验：校验邮箱
     */
    public static final int TAOTAO_REGISTER_CHECK_TYPE_EMAIL = 3;

    /**
     * 订单状态：未付款
     */
    public static final int TB_ORDER_STATUS_NOT_PAY = 1;

    /**
     * 订单状态：已付款
     */
    public static final int TB_ORDER_STATUS_PAYED = 2;

    /**
     * 订单状态：未发货
     */
    public static final int TB_ORDER_STATUS_NOT_SHIPPED = 3;

    /**
     * 订单状态：已发货
     */
    public static final int TB_ORDER_STATUS_SHIPPED = 4;

    /**
     * 订单状态：交易成功
     */
    public static final int TB_ORDER_STATUS_SUCCESS = 5;

    /**
     * 订单状态：交易关闭
     */
    public static final int TB_ORDER_STATUS_CLOSE = 6;
}
