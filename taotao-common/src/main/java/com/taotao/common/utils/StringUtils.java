package com.taotao.common.utils;

/**
 * 字符串处理工具类
 *
 * @author 召
 */

public class StringUtils {

    /**
     * 字符串判断是否为空或空白
     *
     * @param str 字符串
     * @return
     */
    public static boolean isEmpty(String str) {
        return str == null || "".equals(str);
    }

    /**
     * 字符串判断是否是空或空白（trim后）
     *
     * @param str 字符串
     * @return
     */
    public static boolean isBlank(String str) {
        return isEmpty(str) || "".equals(str.trim());
    }
}
