package com.taotao.common.utils;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * 测试httpClient工具类
 *
 * @author 召
 */

public class HttpClientTest {

    /**
     * 测试无参的Get请求
     */
    @Test
    public void testGet() {
        String result = HttpClientUtil.doGet("https://blog.csdn.net/qq_38647878");
        Assert.assertNotNull("应该有数据的呀！",result);
    }

    /**
     * 测试有参的Get请求
     */
    @Test
    public void testGetWithParam() {
        Map<String, String> param = new HashMap<>();
        param.put("t", "1");
        String result = HttpClientUtil.doGet("https://blog.csdn.net/qq_38647878",param);
        Assert.assertNotNull("不应该为空的，应有数据", result);
    }

    /**
     * 测试无参的Post请求
     */
    @Test
    public void testPost() {
        String result = HttpClientUtil.doPost("https://blog.csdn.net/qq_38647878");
        Assert.assertNotNull("应该有数据的呀！",result);
    }

    /**
     * 测试有参的Post请求
     */
    @Test
    public void testPostWithParam() {
        Map<String, String> param = new HashMap<>();
        param.put("t", "1");
        String result = HttpClientUtil.doPost("https://blog.csdn.net/qq_38647878",param);
        Assert.assertNotNull("不应该为空的，应有数据", result);
    }
}
