package com.taotao.common.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * 测试StringUtils工具类
 *
 * @author 召
 */

public class StringUtilsTest {

    @Test
    public void testIsEmpty() {
        Assert.assertTrue("应该为真！",StringUtils.isEmpty(null));
        Assert.assertTrue("应该为真！", StringUtils.isEmpty(""));
        Assert.assertFalse("应该为假！", StringUtils.isEmpty("   "));
        Assert.assertFalse("应该为假!", StringUtils.isEmpty("\t"));
    }

    @Test
    public void testIsBlank() {
        Assert.assertTrue("应该为真！",StringUtils.isBlank(null));
        Assert.assertTrue("应该为真！", StringUtils.isBlank(""));
        Assert.assertTrue("应该为真！", StringUtils.isBlank("   "));
        Assert.assertTrue("应该为真!", StringUtils.isBlank("\t"));
    }
}
