package com.taotao.order.service;

import com.taotao.common.pojo.TaotaoResult;

/**
 * @author 召
 */

public interface OrderService {

    /**
     * 创建订单
     *
     * @param orderJson 订单信息
     * @param token     用户登录凭证
     * @return
     */
    TaotaoResult createOrder(String orderJson, String token);
}
