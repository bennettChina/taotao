package com.taotao.order.service.impl;

import com.taotao.common.pojo.SystemConstants;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.ExceptionUtil;
import com.taotao.common.utils.IDUtils;
import com.taotao.common.utils.JsonUtils;
import com.taotao.common.utils.StringUtils;
import com.taotao.manager.mapper.TbOrderItemMapper;
import com.taotao.manager.mapper.TbOrderMapper;
import com.taotao.manager.mapper.TbOrderShippingMapper;
import com.taotao.manager.pojo.TbOrderExt;
import com.taotao.manager.pojo.TbOrderItem;
import com.taotao.manager.pojo.TbOrderShipping;
import com.taotao.manager.pojo.TbUser;
import com.taotao.order.pojo.OrderResult;
import com.taotao.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author 召
 */

@Slf4j
@Service
@PropertySource("classpath:/resource.properties")
public class OrderServiceImpl implements OrderService {

    @Autowired
    private TbOrderMapper tbOrderMapper;
    @Autowired
    private TbOrderShippingMapper tbOrderShippingMapper;
    @Autowired
    private TbOrderItemMapper tbOrderItemMapper;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Value("${taotao.redis.login.token.prefix}")
    private String loginTokenPrefix;
    @Value("${taotao.sso.login.token.name}")
    private String loginTokenName;


    /**
     * 创建订单
     *
     * @param orderJson 订单信息
     * @param token     用户登录凭证
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public TaotaoResult createOrder(String orderJson, String token) {
        if (StringUtils.isBlank(orderJson)) {
            return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_BAD_REQUEST, "创建订单失败，订单信息不能为空！");
        }

        TbOrderExt orderExt;
        try {
            orderExt = JsonUtils.jsonToPojo(orderJson, TbOrderExt.class);
        } catch (Exception e) {
            log.error("转化订单信息异常!", e);
            return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "转化订单信息异常!", ExceptionUtil.getStackTrace(e));
        }

        try {
            // 从cookie中获取用户id
            if (orderExt != null) {
                // 设置用户id
                TbUser tbUser = getUserIdFromCookie(token);
                orderExt.setUserId(tbUser.getId());
                if (StringUtils.isBlank(orderExt.getBuyerNick())) {
                    orderExt.setBuyerNick(tbUser.getUsername());
                }
                // 保存到数据库，并返回结果
                return TaotaoResult.ok(saveOrder2Db(orderExt));
            }
        } catch (Exception e) {
            log.error("保存订单信息失败！", e);
            throw e;
        }
        return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "转化订单信息异常!");
    }

    /**
     * 获取用户信息
     *
     * @param token 用户登录凭证
     * @return
     */
    private TbUser getUserIdFromCookie(String token) {
        try {
            String json = redisTemplate.opsForValue().get(loginTokenPrefix + token);
            return JsonUtils.jsonToPojo(json, TbUser.class);
        } catch (Exception e) {
            log.error("获取用户信息出错！", e);
        }
        return null;
    }

    private OrderResult saveOrder2Db(TbOrderExt orderExt) {
        // 生成订单号
        String orderId = String.valueOf(IDUtils.genItemId());

        // 完善订单表的信息
        orderExt.setOrderId(orderId);
        // 初始化订单创建时间、更新时间
        Date date = new Date();
        orderExt.setCreateTime(date);
        orderExt.setUpdateTime(date);
        orderExt.setStatus(SystemConstants.TB_ORDER_STATUS_PAYED);
        orderExt.setShippingCode(IDUtils.genImageName());

        // 插入订单表
        tbOrderMapper.insert(orderExt);
        // 插入订单商品表
        for (TbOrderItem orderItem : orderExt.getOrderItems()) {
            // 完善订单商品表信息
            orderItem.setId(IDUtils.genImageName());
            orderItem.setOrderId(orderId);

            // 添加到订单商品表
            tbOrderItemMapper.insert(orderItem);
        }

        // 完善物流信息
        TbOrderShipping orderShipping = orderExt.getOrderShipping();
        orderShipping.setOrderId(orderId);
        orderShipping.setCreated(date);
        orderShipping.setUpdated(date);

        // 添加到物流表
        tbOrderShippingMapper.insert(orderShipping);

        // 初始化结果
        OrderResult result = new OrderResult();
        // 设置返回值
        result.setOrderId(orderId);
        result.setTotalPrice(Double.valueOf(orderExt.getPayment()));
        // 设置快递送达时间
        result.setDate(new DateTime(DateTimeZone.UTC).plusDays(2).toString("yyyy-MM-dd"));
        return result;
    }

}
