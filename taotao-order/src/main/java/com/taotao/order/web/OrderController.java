package com.taotao.order.web;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.ExceptionUtil;
import com.taotao.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 处理用户的订单服务
 *
 * @author 召
 */

@Slf4j
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 创建订单
     *
     * @param orderJson 订单信息
     * @param token     用户登录凭证
     * @return
     */
    @PostMapping("/create")
    public TaotaoResult createOrder(@RequestParam("data") String orderJson, String token) {
        TaotaoResult result;
        try {
            result = orderService.createOrder(orderJson, token);
        } catch (Exception e) {
            return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "保存订单出错了！", ExceptionUtil.getStackTrace(e));
        }
        return result;
    }
}
