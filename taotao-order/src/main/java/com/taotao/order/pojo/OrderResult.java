package com.taotao.order.pojo;

import lombok.Getter;
import lombok.Setter;

/**
*
*@author 召
*/

@Setter
@Getter
public class OrderResult {

    /**
     * 商品id
     */
    private String orderId;

    /**
     * 订单总价
     */
    private Double totalPrice;

    /**
     * 物流送达日期
     */
    private String date = "明天";
}
