package com.taotao.manager.mapper;

import com.taotao.manager.pojo.SearchItem;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 召
 */

@Repository
public interface SearchItemMapper {

    /**
     * 查询所有的商品信息
     *
     * @return
     */
    List<SearchItem> selectAllItem();
}
