package com.taotao.manager.web;

import com.taotao.common.pojo.SystemConstants;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.manager.pojo.TbItem;
import com.taotao.manager.service.TbItemParamService;
import com.taotao.manager.service.TbItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * restFul服务
 *
 * @author 召
 */

@RestController
@RequestMapping("/rest/item")
public class RestFulController {

    @Autowired
    private TbItemService tbItemService;
    @Autowired
    private TbItemParamService tbItemParamService;

    /**
     * 查询商品的描述信息
     *
     * @param id 商品id
     * @return
     */
    @RequestMapping("/query/item/desc/{id}")
    public TaotaoResult queryItemDesc(@PathVariable("id") Long id) {
        return tbItemService.queryItemDesc(id);
    }

    /**
     * 根据id查询对应的商品规格
     *
     * @param id 商品id
     * @return
     */
    @RequestMapping("/param/item/query/{id}")
    public TaotaoResult queryItemParamItem(@PathVariable("id") Long id) {
        return tbItemParamService.queryTbItemParamItem(id);
    }

    /**
     * 更新商品信息
     *
     * @param tbItem      商品
     * @param desc        商品描述信息
     * @param itemParams  商品的规则参数
     * @param itemParamId 规则参数id
     * @return
     */
    @RequestMapping("/update")
    public TaotaoResult updateItem(TbItem tbItem, @RequestParam("desc") String desc,
                                   @RequestParam("itemParams") String itemParams,
                                   @RequestParam("itemParamId") Long itemParamId) {
        return tbItemService.updateItem(tbItem, desc, itemParams, itemParamId);
    }

    /**
     * 删除商品
     *
     * @param itemId 商品id
     * @return
     */
    @RequestMapping("/delete")
    public TaotaoResult deleteTbItem(@RequestParam("ids") Long[] itemId) {
        return tbItemService.updateItemStatus(itemId, SystemConstants.TB_ITEM_STATUS_DELETED);
    }

    /**
     * 下架商品
     *
     * @param itemId 商品id
     * @return
     */
    @RequestMapping("/instock")
    public TaotaoResult inStockTbItem(@RequestParam("ids") Long[] itemId) {
        return tbItemService.updateItemStatus(itemId, SystemConstants.TB_ITEM_STATUS_DISCONTINUED);
    }

    /**
     * 上架商品
     *
     * @param itemId 商品id
     * @return
     */
    @RequestMapping("/reshelf")
    public TaotaoResult reShelfTbItem(@RequestParam("ids") Long[] itemId) {
        return tbItemService.updateItemStatus(itemId, SystemConstants.TB_ITEM_STATUS_NORMAL);
    }
}
