package com.taotao.manager.web;

import com.taotao.common.pojo.EasyUiResult;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.manager.pojo.TbContent;
import com.taotao.manager.service.TbContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 内容管理相关请求处理
 *
 * @author 召
 */

@RestController
@RequestMapping("/content")
public class TbContentController {

    @Autowired
    private TbContentService tbContentService;

    @RequestMapping("/query/list")
    public EasyUiResult getContentList(@RequestParam("categoryId") Long categoryId, @RequestParam("page") int pageNum, @RequestParam("rows") int pageSize) {
        return tbContentService.getContentList(categoryId, pageNum, pageSize);
    }

    /**
     * 新增类目内容
     *
     * @param tbContent 内容
     * @return
     */
    @RequestMapping("/save")
    public TaotaoResult saveContentList(TbContent tbContent) {
        return tbContentService.saveContentList(tbContent);
    }

    /**
     * 编辑类目内容
     *
     * @param tbContent 内容
     * @return
     */
    @RequestMapping("/edit")
    public TaotaoResult editContentList(TbContent tbContent) {
        return tbContentService.editContentList(tbContent);
    }

    /**
     * 删除类目内容（批量）
     *
     * @param ids 内容id
     * @return
     */
    @RequestMapping("/delete")
    public TaotaoResult deleteContentList(@RequestParam("ids") Long[] ids) {
        return tbContentService.deleteContentList(ids);
    }
}
