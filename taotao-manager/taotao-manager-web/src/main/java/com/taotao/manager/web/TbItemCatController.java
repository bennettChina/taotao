package com.taotao.manager.web;

import com.taotao.common.pojo.EuTreeNode;
import com.taotao.manager.service.TbItemCatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 商品类目相关请求的处理类
 *
 * @author 召
 */

@RestController
@RequestMapping("/item/cat")
public class TbItemCatController {

    @Autowired
    private TbItemCatService tbItemCatService;

    /**
     * 获取规则参数异步树
     *
     * @param parentId
     * @return
     */
    @RequestMapping("/list")
    public List<EuTreeNode> getTbItemCatList(@RequestParam(value = "id", defaultValue = "0") long parentId) {
        List<EuTreeNode> result = tbItemCatService.getItemCategoryList(parentId);
        return result;
    }
}
