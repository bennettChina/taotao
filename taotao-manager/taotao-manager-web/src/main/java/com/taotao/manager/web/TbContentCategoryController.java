package com.taotao.manager.web;

import com.taotao.common.pojo.EuTreeNode;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.manager.service.TbContentCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 内容分类管理相关请求处理
 *
 * @author 召
 */

@RestController
@RequestMapping("/content/category")
public class TbContentCategoryController {

    @Autowired
    private TbContentCategoryService tbContentCategoryService;

    /**
     * 获取内容分类异步树
     *
     * @param parentId 父类目ID
     * @return
     */
    @RequestMapping("/list")
    public List<EuTreeNode> getCategoryList(@RequestParam(value = "id", defaultValue = "0") Long parentId) {
        return tbContentCategoryService.getCategoryList(parentId);
    }

    /**
     * 创建内容类目的节点
     *
     * @param parentId 父类目ID
     * @param name     类目的名称
     * @return
     */
    @RequestMapping("/create")
    public TaotaoResult createCategory(@RequestParam("parentId") Long parentId, @RequestParam("name") String name) {
        return tbContentCategoryService.createCategory(parentId, name);
    }

    /**
     * 重命名类目信息
     *
     * @param id   类目id
     * @param name 类目名称
     * @return
     */
    @RequestMapping("/update")
    public TaotaoResult updateCategory(@RequestParam("id") Long id, @RequestParam("name") String name) {
        if (name == null || "".equals(name)) {
            return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "操作异常！");
        }
        return tbContentCategoryService.updateCategory(id, name);
    }

    /**
     * 删除类目
     *
     * @param id 类目id
     * @return
     */
    @RequestMapping("/delete")
    public TaotaoResult deleteCategory(@RequestParam("id") Long id) {
        return tbContentCategoryService.updateCategory(id);
    }
}
