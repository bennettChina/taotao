package com.taotao.manager.web;

import com.taotao.common.pojo.EasyUiResult;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.manager.pojo.TbItem;
import com.taotao.manager.service.TbItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商品相关请求的处理类
 *
 * @author 召
 */

@RestController
@RequestMapping("/item")
public class TbItemController {

    @Autowired
    private TbItemService tbItemService;

    /**
     * 分页查询商品列表
     *
     * @param pageNum  当前页
     * @param pageSize 每页的数量
     * @return
     */
    @RequestMapping("/list")
    public EasyUiResult getTbItemList(@RequestParam("page") int pageNum, @RequestParam("rows") int pageSize) {
        EasyUiResult result = tbItemService.getItemList(pageNum, pageSize);
        return result;
    }

    /**
     * 新增商品
     *
     * @param tbItem 商品信息
     * @return
     */
    @RequestMapping("/save")
    public TaotaoResult saveTbItem(TbItem tbItem, @RequestParam("desc") String desc,
                                   @RequestParam("itemParams") String itemParams) {
        TaotaoResult result = tbItemService.saveTbItem(tbItem, desc, itemParams);
        return result;
    }

}
