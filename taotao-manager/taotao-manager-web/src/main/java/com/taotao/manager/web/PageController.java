package com.taotao.manager.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 页面跳转处理类
 *
 * @author 召
 */

@Controller
public class PageController {

    @RequestMapping("page/{page}")
    public String page(@PathVariable("page") String page) {
        return page;
    }
}
