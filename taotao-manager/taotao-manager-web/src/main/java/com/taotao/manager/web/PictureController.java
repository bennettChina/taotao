package com.taotao.manager.web;

import com.taotao.common.utils.ExceptionUtil;
import com.taotao.manager.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * 图片上传处理类
 *
 * @author 召
 */

@RestController
public class PictureController {

    @Autowired
    private PictureService pictureService;

    @RequestMapping("/pic/upload")
    public Map<String, Object> uploadPic(@RequestParam("uploadFile") MultipartFile picFile) {
        Map<String, Object> result = new HashMap<>();
        String picName = picFile.getOriginalFilename();
        try {
            InputStream file = picFile.getInputStream();
            result = pictureService.uploadPicture(picName, file);
        } catch (IOException e) {
            result.put("error", 1);
            result.put("message", "上传失败！" + ExceptionUtil.getStackTrace(e));
        }
        return result;
    }
}
