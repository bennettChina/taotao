package com.taotao.manager.web;

import com.taotao.common.pojo.EasyUiResult;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.manager.service.TbItemParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 规格参数相关请求的处理类
 *
 * @author 召
 */

@RestController
@RequestMapping("/item/param")
public class TbItemParamController {

    @Autowired
    private TbItemParamService tbItemParamService;

    /**
     * 获取规则参数列表（分页）
     *
     * @param pageNum  当前页码
     * @param pageSize 每页数据量
     * @return
     */
    @RequestMapping("/list")
    public EasyUiResult getTbItemParamList(@RequestParam("page") int pageNum, @RequestParam("rows") int pageSize) {
        return tbItemParamService.getTbItemParamList(pageNum, pageSize);
    }

    /**
     * 查询规则参数
     *
     * @param id 商品类目id
     * @return
     */
    @RequestMapping("/query/itemcatid/{id}")
    public TaotaoResult queryTbItemParam(@PathVariable("id") long id) {
        return tbItemParamService.queryTbItemParam(id);
    }

    /**
     * 新增规则参数
     *
     * @param id        商品类目id
     * @param paramData 规则参数
     * @return
     */
    @RequestMapping("/save/{id}")
    public TaotaoResult saveTbItemParam(@PathVariable("id") long id, @RequestParam("paramData") String paramData) {
        return tbItemParamService.saveTbItemParam(id, paramData);
    }

    /**
     * 删除规则参数
     *
     * @param id 规则参数id
     * @return
     */
    @RequestMapping("/delete")
    public TaotaoResult deleteTbItemParam(@RequestParam("ids") long[] id) {
        return tbItemParamService.deleteTbItemParam(id);
    }
}
