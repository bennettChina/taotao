package com.taotao.manager.web;

import com.taotao.common.utils.FtpUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
*
*@author 召
*/

@RunWith(SpringRunner.class)
@SpringBootTest
public class PicUploadTest {

    @Test
    public void testFtpClient() throws FileNotFoundException {

        String host = "10.10.14.163";
        int port = 21;
        String user = "ftpuser";
        String password = "123456";
        String basePath = "/home/ftpuser";
        String filePath = "20190726";
        String fileName = System.currentTimeMillis() + ".txt";
        InputStream file = new FileInputStream("D:/test.txt");

        boolean ret = FtpUtil.uploadFile(host, port, user, password, basePath, filePath, fileName, file);
        Assert.assertTrue("上传失败", ret);
        System.out.println(ret);
    }


}
