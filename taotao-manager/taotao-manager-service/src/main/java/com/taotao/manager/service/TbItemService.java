package com.taotao.manager.service;

import com.taotao.common.pojo.EasyUiResult;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.manager.pojo.TbItem;

/**
 * @author 召
 */

public interface TbItemService {

    /**
     * 获取商品列表（分页后的）
     *
     * @param pageNum  当前页
     * @param pageSize 每页的数据量
     * @return
     */
    EasyUiResult getItemList(int pageNum, int pageSize);

    /**
     * 新增商品
     *
     * @param tbItem     商品
     * @param desc       商品的描述信息
     * @param itemParams 商品规则参数
     * @return 响应数据、响应信息、响应状态码
     */
    TaotaoResult saveTbItem(TbItem tbItem, String desc, String itemParams);

    /**
     * 查询商品的描述信息
     *
     * @param id 商品的id
     * @return
     */
    TaotaoResult queryItemDesc(long id);

    /**
     * 更新商品信息
     *
     * @param tbItem      商品信息
     * @param desc        商品描述信息
     * @param itemParams  商品规格参数
     * @param itemParamId 规格参数id
     * @return
     */
    TaotaoResult updateItem(TbItem tbItem, String desc, String itemParams, Long itemParamId);

    /**
     * 更新商品的状态
     *
     * @param itemId 商品id
     * @param status 商品状态
     * @return
     */
    TaotaoResult updateItemStatus(Long[] itemId, Byte status);
}
