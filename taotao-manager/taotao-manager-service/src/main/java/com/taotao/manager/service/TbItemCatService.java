package com.taotao.manager.service;

import com.taotao.common.pojo.EuTreeNode;

import java.util.List;

/**
 * @author 召
 */

public interface TbItemCatService {

    /**
     * 获取异步树的列表
     *
     * @param parentId
     * @return
     */
    List<EuTreeNode> getItemCategoryList(long parentId);
}
