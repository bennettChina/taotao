package com.taotao.manager.service;

import com.taotao.common.pojo.EuTreeNode;
import com.taotao.common.pojo.TaotaoResult;

import java.util.List;

/**
 * 内容分类管理相关业务逻辑接口类
 *
 * @author 召
 */

public interface TbContentCategoryService {

    /**
     * 获取内容分类异步树
     *
     * @param parentId 父类目ID
     * @return
     */
    List<EuTreeNode> getCategoryList(Long parentId);

    /**
     * 创建内容类目的节点
     *
     * @param parentId 父类目ID
     * @param name     节点名
     * @return
     */
    TaotaoResult createCategory(Long parentId, String name);

    /**
     * 重命名类目
     *
     * @param id   类目id
     * @param name 类目名称
     * @return
     */
    TaotaoResult updateCategory(Long id, String name);

    /**
     * 删除类目
     * @param id 类目id
     * @return
     */
    TaotaoResult updateCategory(Long id);
}
