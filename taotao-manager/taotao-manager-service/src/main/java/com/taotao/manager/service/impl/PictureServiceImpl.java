package com.taotao.manager.service.impl;

import com.taotao.common.pojo.SystemConstants;
import com.taotao.common.utils.ExceptionUtil;
import com.taotao.common.utils.FtpUtil;
import com.taotao.common.utils.IDUtils;
import com.taotao.manager.service.PictureService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 召
 */

@Service
@PropertySource("classpath:/resource.properties")
public class PictureServiceImpl implements PictureService {

    @Value("${ftp.host}")
    private String ftpHost;
    @Value("${ftp.port}")
    private Integer ftpPort;
    @Value("${ftp.username}")
    private String ftpUsername;
    @Value("${ftp.password}")
    private String ftpPassword;
    @Value("${ftp.basePath}")
    private String ftpBasePath;
    @Value("${image.domain}")
    private String imageDomain;

    @Override
    public Map<String, Object> uploadPicture(String fileName, InputStream picFile) {
        Map<String, Object> result = new HashMap<>();

        // 获取上传的文件后缀名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));

        // 将图片上传到FTP服务器
        fileName = IDUtils.genImageName() + suffixName;

        SimpleDateFormat sdf = new SimpleDateFormat(SystemConstants.DATE_FORMAT);
        // 文件上传路径:2019/7/27
        String filePath = sdf.format(System.currentTimeMillis());

        StringBuilder fileUrl = new StringBuilder();
        fileUrl.append(imageDomain);
        fileUrl.append("/");
        fileUrl.append(filePath);
        fileUrl.append("/");
        fileUrl.append(fileName);

        try {
            boolean flag = FtpUtil.uploadFile(ftpHost, ftpPort, ftpUsername, ftpPassword, ftpBasePath, filePath, fileName, picFile);
            if (!flag) {
                result.put("error", 1);
                result.put("message", "上传失败！");
            } else {
                result.put("error", 0);
                result.put("url", fileUrl.toString());
            }
        } catch (Exception e) {
            result.put("error", 1);
            result.put("message", "上传失败！" + ExceptionUtil.getStackTrace(e));
        }
        return result;
    }
}
