package com.taotao.manager.service;

import com.taotao.common.pojo.EasyUiResult;
import com.taotao.common.pojo.TaotaoResult;

/**
 * 规格参数相关业务逻辑接口类
 *
 * @author 召
 */

public interface TbItemParamService {

    /**
     * 分页查询规格参数列表
     *
     * @param pageNum  当前页码
     * @param pageSize 每页数据量
     * @return
     */
    EasyUiResult getTbItemParamList(int pageNum, int pageSize);

    /**
     * 根据id查询对应的商品规格
     *
     * @param id 商品id
     * @return
     */
    TaotaoResult queryTbItemParamItem(long id);

    /**
     * 根据id查询对应的商品规则参数
     *
     * @param id 商品类目id
     * @return
     */
    TaotaoResult queryTbItemParam(long id);

    /**
     * 新增规则参数
     *
     * @param id        商品类目id
     * @param paramData 规则参数
     * @return
     */
    TaotaoResult saveTbItemParam(long id, String paramData);

    /**
     * 删除规则参数
     *
     * @param id 规则参数id
     * @return
     */
    TaotaoResult deleteTbItemParam(long[] id);
}
