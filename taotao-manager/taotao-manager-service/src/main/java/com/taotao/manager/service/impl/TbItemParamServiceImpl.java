package com.taotao.manager.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.taotao.common.pojo.EasyUiResult;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.HttpClientUtil;
import com.taotao.manager.mapper.TbItemCatMapper;
import com.taotao.manager.mapper.TbItemParamItemMapper;
import com.taotao.manager.mapper.TbItemParamMapper;
import com.taotao.manager.pojo.*;
import com.taotao.manager.service.TbItemParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 规格参数相关业务逻辑实现类
 *
 * @author 召
 */

@PropertySource("classpath:/rest_ful.properties")
@Service
public class TbItemParamServiceImpl implements TbItemParamService {

    @Autowired
    private TbItemParamMapper tbItemParamMapper;
    @Autowired
    private TbItemCatMapper tbItemCatMapper;
    @Autowired
    private TbItemParamItemMapper tbItemParamItemMapper;

    @Value("${taotao.rest.host}")
    private String host;
    @Value("${taotao.rest.url.itemCat}")
    private String url;

    @Override
    public EasyUiResult getTbItemParamList(int pageNum, int pageSize) {
        /*
         * 需求的数据：id，itemCatId，itemCatName，paramData，created，updated
         * */

        // 分页查询数据
        PageHelper.startPage(pageNum, pageSize);
        Page<TbItemParam> paramPage = (Page<TbItemParam>) tbItemParamMapper.selectByExampleWithBLOBs(new TbItemParamExample());
        List<TbItemCat> itemCatPage = tbItemCatMapper.selectByExample(new TbItemCatExample());

        // 连表查询功能
        for (TbItemParam param : paramPage) {
            for (TbItemCat cat : itemCatPage) {
                if (param.getItemCatId().equals(cat.getId())) {
                    param.setItemCatName(cat.getName());
                }
            }
        }

        // 设置返回的数据
        long total = paramPage.getTotal();
        return new EasyUiResult(total, paramPage.getResult());
    }

    @Override
    public TaotaoResult queryTbItemParamItem(long id) {
        // 创建查询条件
        TbItemParamItemExample example = new TbItemParamItemExample();
        example.createCriteria().andItemIdEqualTo(id);

        List<TbItemParamItem> itemParamItemList = tbItemParamItemMapper.selectByExampleWithBLOBs(example);

        if (itemParamItemList != null && itemParamItemList.size() > 0) {
            return TaotaoResult.ok(itemParamItemList.get(0));
        }
        return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "该商品没有添加规格！");
    }

    @Override
    public TaotaoResult queryTbItemParam(long id) {
        // 创建查询条件
        TbItemParamExample example = new TbItemParamExample();
        example.createCriteria().andItemCatIdEqualTo(id);

        List<TbItemParam> itemParamList = tbItemParamMapper.selectByExampleWithBLOBs(example);

        if (itemParamList != null && itemParamList.size() > 0) {
            return TaotaoResult.ok(itemParamList.get(0));
        }
        return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "该类目没有规则！");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public TaotaoResult saveTbItemParam(long id, String paramData) {
        // 获取一个日期时间
        Date date = new Date();

        //创建新增的规则信息
        TbItemParam tbItemParam = new TbItemParam();
        tbItemParam.setItemCatId(id);
        tbItemParam.setParamData(paramData);
        tbItemParam.setCreated(date);
        tbItemParam.setUpdated(date);

        // 执行新增
        long rows = tbItemParamMapper.insert(tbItemParam);

        if (rows > 0) {
            // 清理缓存，做缓存同步
            Map<String, String> params = new HashMap<>(16);
            params.put("ids",id + "");
            HttpClientUtil.doGet(host + url, params);
            return TaotaoResult.ok();
        }
        return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "添加失败！");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public TaotaoResult deleteTbItemParam(long[] id) {
        long rows = 0;
        for (int i = 0; i < id.length; i++) {
            rows += tbItemParamMapper.deleteByPrimaryKey(id[i]);
        }
        if (rows > 0) {
            // 清理缓存，做缓存同步
            Map<String, String> params = new HashMap<>(16);
            // 将long数组类型的id重新转化为字符串
            StringBuilder stringBuilder = new StringBuilder();
            int count = 0;
            for (Long temp : id) {
                stringBuilder.append(temp);
                if (count < id.length - 1) {
                    stringBuilder.append(",");
                }
                count ++;
            }
            params.put("ids",stringBuilder.toString());
            HttpClientUtil.doGet(host + url, params);
            return TaotaoResult.ok();
        }
        return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "删除失败！");
    }
}
