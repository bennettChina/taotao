package com.taotao.manager.service;

import java.io.InputStream;
import java.util.Map;

/**
 * 图片上传功能
 *
 * @author 召
 */

public interface PictureService {

    /**
     * 上传图片
     *
     * @param fileName 文件名
     * @param picFile  文件本身
     * @return 成功返回访问url，失败返回错误信息
     */
    Map<String, Object> uploadPicture(String fileName, InputStream picFile);
}
