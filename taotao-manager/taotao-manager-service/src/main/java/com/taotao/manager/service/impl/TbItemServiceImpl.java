package com.taotao.manager.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.taotao.common.pojo.EasyUiResult;
import com.taotao.common.pojo.SystemConstants;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.HttpClientUtil;
import com.taotao.common.utils.IDUtils;
import com.taotao.manager.mapper.TbItemDescMapper;
import com.taotao.manager.mapper.TbItemMapper;
import com.taotao.manager.mapper.TbItemParamItemMapper;
import com.taotao.manager.mapper.TbItemParamMapper;
import com.taotao.manager.pojo.*;
import com.taotao.manager.service.TbItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 商品的业务实现类
 *
 * @author 召
 */

@PropertySource("classpath:/rest_ful.properties")
@Service
public class TbItemServiceImpl implements TbItemService {

    @Autowired
    private TbItemMapper tbItemMapper;
    @Autowired
    private TbItemDescMapper tbItemDescMapper;
    @Autowired
    private TbItemParamMapper tbItemParamMapper;
    @Autowired
    private TbItemParamItemMapper tbItemParamItemMapper;

    @Value("${taotao.rest.host}")
    private String host;
    @Value("${taotao.rest.url.item}")
    private String url;
    @Value("${taotao.rest.url.itemDesc}")
    private String descUrl;
    @Value("${taotao.rest.url.itemParamItem}")
    private String paramUrl;


    @Override
    public EasyUiResult getItemList(int pageNum, int pageSize) {
        // 设置分页参数
        PageHelper.startPage(pageNum, pageSize);

        Page<TbItem> page = (Page<TbItem>) tbItemMapper.selectByExample(new TbItemExample());
        long total = page.getTotal();

        return new EasyUiResult(total, page.getResult());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public TaotaoResult saveTbItem(TbItem tbItem, String desc, String itemParams) {
        // 设置商品id
        long itemId = IDUtils.genItemId();
        tbItem.setId(itemId);
        // 设置商品状态
        tbItem.setStatus(SystemConstants.TB_ITEM_STATUS_NORMAL);
        // 设置商品添加时间
        Date date = new Date();
        tbItem.setCreated(date);
        tbItem.setUpdated(date);

        // 添加tb_item表的数据
        int rows = tbItemMapper.insertSelective(tbItem);

        // 添加tb_item_desc表的数据
        TbItemDesc tbItemDesc = new TbItemDesc();
        tbItemDesc.setCreated(date);
        tbItemDesc.setUpdated(date);
        tbItemDesc.setItemDesc(desc);
        tbItemDesc.setItemId(itemId);
        int descRows = tbItemDescMapper.insert(tbItemDesc);

        // 添加tb_item_param_item表的数据
        TbItemParamItem itemParamItem = new TbItemParamItem();
        itemParamItem.setItemId(itemId);
        itemParamItem.setParamData(itemParams);
        itemParamItem.setCreated(date);
        itemParamItem.setUpdated(date);
        long itemParamItemRows = tbItemParamItemMapper.insert(itemParamItem);

        // 设置响应状态、信息
        if (rows > 0 && descRows > 0 && itemParamItemRows > 0) {
            // 清理缓存，做缓存同步
            Map<String, String> params = new HashMap<>(16);
            params.put("ids", itemId + "");
            HttpClientUtil.doGet(host + url, params);
            return TaotaoResult.ok();
        }
        return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "添加失败！");
    }

    @Override
    public TaotaoResult queryItemDesc(long id) {
        TbItemDesc itemDesc = tbItemDescMapper.selectByPrimaryKey(id);
        if (itemDesc != null) {
            return TaotaoResult.ok(itemDesc);
        }
        return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "没有描述信息!");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public TaotaoResult updateItem(TbItem tbItem, String desc, String itemParams, Long itemParamId) {
        Date date = new Date();
        // 更新tb_item表
        tbItem.setUpdated(date);
        int rows = tbItemMapper.updateByPrimaryKeySelective(tbItem);

        // 清理tb_item表缓存
        if (rows > 0) {
            Map<String, String> params = new HashMap<>(16);
            params.put("ids", tbItem.getId().toString());
            HttpClientUtil.doGet(host + url, params);
        }

        // 更新tb_desc表
        TbItemDesc itemDesc = new TbItemDesc();
        itemDesc.setUpdated(date);
        itemDesc.setItemDesc(desc);
        itemDesc.setItemId(tbItem.getId());
        int descRows = tbItemDescMapper.updateByPrimaryKeySelective(itemDesc);

        // 清理tb_desc表缓存
        if (descRows > 0) {
            Map<String, String> params = new HashMap<>(16);
            params.put("ids", tbItem.getId().toString());
            HttpClientUtil.doGet(host + descUrl, params);
        }

        // 更新tb_item_param表
        boolean isNotBlank = itemParamId != null && !"".equals(itemParamId);
        if (isNotBlank) {
            TbItemParam itemParam = new TbItemParam();
            itemParam.setId(itemParamId);
            itemParam.setParamData(itemParams);
            itemParam.setUpdated(date);
            int paramRows = tbItemParamMapper.updateByPrimaryKeySelective(itemParam);
            // 清理tb_desc表缓存
            if (paramRows > 0) {
                Map<String, String> params = new HashMap<>(16);
                params.put("ids", tbItem.getId().toString());
                HttpClientUtil.doGet(host + paramUrl, params);
            }
        }

        return TaotaoResult.ok();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public TaotaoResult updateItemStatus(Long[] itemId, Byte status) {
        boolean isNotBlank = itemId != null && !"".equals(itemId);
        int rows = 0;
        if (isNotBlank) {
            TbItem tbItem = new TbItem();
            // 设置需要更新的信息
            tbItem.setStatus(status);
            for (int i = 0; i < itemId.length; i++) {
                tbItem.setId(itemId[i]);
                tbItem.setUpdated(new Date());
                rows += tbItemMapper.updateByPrimaryKeySelective(tbItem);
            }
        }

        // 清理缓存
        if (rows > 0) {
            Map<String, String> params = new HashMap<>(16);
            // 将long数组类型的id重新转化为字符串
            StringBuilder stringBuilder = new StringBuilder();
            int count = 0;
            for (Long id : itemId) {
                stringBuilder.append(id);
                if (count < itemId.length - 1) {
                    stringBuilder.append(",");
                }
                count ++;
            }
            params.put("ids", stringBuilder.toString());
            HttpClientUtil.doGet(host + url, params);
            HttpClientUtil.doGet(host + descUrl, params);
            HttpClientUtil.doGet(host + paramUrl, params);
        }

        // 设置响应状态码、信息
        return TaotaoResult.ok();
    }
}
