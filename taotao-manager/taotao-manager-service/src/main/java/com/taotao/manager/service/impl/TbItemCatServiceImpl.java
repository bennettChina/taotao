package com.taotao.manager.service.impl;

import com.taotao.common.pojo.EuTreeNode;
import com.taotao.common.pojo.SystemConstants;
import com.taotao.manager.mapper.TbItemCatMapper;
import com.taotao.manager.pojo.TbItemCat;
import com.taotao.manager.pojo.TbItemCatExample;
import com.taotao.manager.service.TbItemCatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
*
*@author 召
*/

@Service
public class TbItemCatServiceImpl implements TbItemCatService {

    @Autowired
    private TbItemCatMapper tbItemCatMapper;

    @Override
    public List<EuTreeNode> getItemCategoryList(long parentId) {
        // 按照parentId查询分类数据
        TbItemCatExample example = new TbItemCatExample();
        example.createCriteria().andParentIdEqualTo(parentId);
        List<TbItemCat> tbItemCatList = tbItemCatMapper.selectByExample(example);

        List<EuTreeNode> treeNodeList = new ArrayList<>();
        // 把查到的分类数据封装成EasyUI异步树数据结构
        for (TbItemCat itemCat : tbItemCatList) {
            EuTreeNode node = new EuTreeNode();
            node.setId(itemCat.getId());
            node.setState(itemCat.getIsParent() ? SystemConstants.TREE_NODE_STATUS_CLOSE : SystemConstants.TREE_NODE_STATUS_OPEN);
            node.setText(itemCat.getName());
            treeNodeList.add(node);
        }
        return treeNodeList;
    }
}
