package com.taotao.manager.service;

import com.taotao.common.pojo.EasyUiResult;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.manager.pojo.TbContent;

/**
 * 内容管理相关业务逻辑接口类
 *
 * @author 召
 */

public interface TbContentService {

    /**
     * 获取类目内容
     *
     * @param categoryId 类目id
     * @param pageNum    当前页码
     * @param pageSize   每页数据量
     * @return
     */
    EasyUiResult getContentList(Long categoryId, int pageNum, int pageSize);

    /**
     * 新增类目内容
     *
     * @param tbContent 内容
     * @return
     */
    TaotaoResult saveContentList(TbContent tbContent);

    /**
     * 编辑类目内容
     *
     * @param tbContent 内容
     * @return
     */
    TaotaoResult editContentList(TbContent tbContent);

    /**
     * 删除类目内容（批量）
     *
     * @param ids 内容id
     * @return
     */
    TaotaoResult deleteContentList(Long[] ids);
}
