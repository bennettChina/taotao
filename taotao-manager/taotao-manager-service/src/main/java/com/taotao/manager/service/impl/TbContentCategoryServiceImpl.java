package com.taotao.manager.service.impl;

import com.taotao.common.pojo.EuTreeNode;
import com.taotao.common.pojo.SystemConstants;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.manager.mapper.TbContentCategoryMapper;
import com.taotao.manager.pojo.TbContentCategory;
import com.taotao.manager.pojo.TbContentCategoryExample;
import com.taotao.manager.service.TbContentCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 内容分类管理相关业务逻辑实现类
 *
 * @author 召
 */

@Service
public class TbContentCategoryServiceImpl implements TbContentCategoryService {

    @Autowired
    private TbContentCategoryMapper tbContentCategoryMapper;

    @Transactional(readOnly = true)
    @Override
    public List<EuTreeNode> getCategoryList(Long parentId) {
        // 创建查询条件
        TbContentCategoryExample example = new TbContentCategoryExample();
        example.createCriteria().andParentIdEqualTo(parentId)
                .andStatusEqualTo(SystemConstants.TB_CONTENT_CATEGORY_STATUS_NORMAL);

        // 查询数据
        List<TbContentCategory> categoryList = tbContentCategoryMapper.selectByExample(example);

        // 把查到的数据封装成EasyUI异步树数据结构
        List<EuTreeNode> treeNodeList = new ArrayList<>();
        for (TbContentCategory category : categoryList) {
            EuTreeNode node = new EuTreeNode();
            // 添加数据到treeNode中
            node.setId(category.getId());
            node.setState(category.getIsParent() ? SystemConstants.TREE_NODE_STATUS_CLOSE : SystemConstants.TREE_NODE_STATUS_OPEN);
            node.setText(category.getName());
            treeNodeList.add(node);
        }

        return treeNodeList;
    }

    @Transactional(rollbackFor = java.lang.Exception.class)
    @Override
    public TaotaoResult createCategory(Long parentId, String name) {
        // 获取日期时间
        Date date = new Date();
        // 创建添加条件
        TbContentCategory category = new TbContentCategory();
        category.setParentId(parentId);
        category.setName(name);
        category.setIsParent(false);
        category.setStatus(SystemConstants.TB_CONTENT_CATEGORY_STATUS_NORMAL);
        category.setSortOrder(SystemConstants.TB_CONTENT_CATEGORY_SORT);
        category.setCreated(date);
        category.setUpdated(date);
        // 添加分类
        long rows = tbContentCategoryMapper.insert(category);

        // 判断添加到的类目是否已经是父类目，若不是则更新为父类目
        if (!isParent(parentId)){
            // 更新父类目的状态为父类目
            TbContentCategory condition = new TbContentCategory();
            condition.setIsParent(true);
            condition.setId(parentId);
            condition.setUpdated(date);
            long updateRows = tbContentCategoryMapper.updateByPrimaryKeySelective(condition);
            // 设置响应状态、信息、数据
            if (rows > 0 && updateRows > 0) {
                return TaotaoResult.ok(category);
            }
        }

        // 设置响应状态、信息、数据
        if (rows > 0) {
            return TaotaoResult.ok(category);
        }
        return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "添加失败！");
    }

    @Transactional(rollbackFor = java.lang.Exception.class)
    @Override
    public TaotaoResult updateCategory(Long id, String name) {
        // 判断操作是否是正常操作
        if (id == null) {
            return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "操作异常！");
        }

        // 判断name是否存在，若不存在则是删除类目
        TbContentCategory category = new TbContentCategory();
        category.setId(id);
        category.setUpdated(new Date());
        if (name == null || "".equals(name)) {
            // 删除类目
            category.setStatus(SystemConstants.TB_CONTENT_CATEGORY_STATUS_DELETED);
            // 删除此类目
            int rows = tbContentCategoryMapper.updateByPrimaryKeySelective(category);

            // 查询此类目对应的父类目信息
            TbContentCategory temp = tbContentCategoryMapper.selectByPrimaryKey(id);

            // 查询父类目还有多少子类目
            TbContentCategoryExample example = new TbContentCategoryExample();
            example.createCriteria().andParentIdEqualTo(temp.getParentId())
                    .andStatusEqualTo(SystemConstants.TB_CONTENT_CATEGORY_STATUS_NORMAL);
            List<TbContentCategory> categoryList = tbContentCategoryMapper.selectByExample(example);

            // 如果父类目已经没有子类目，则更新父类目为子类目
            if (categoryList.size() == 0){
                TbContentCategory condition = new TbContentCategory();
                condition.setId(temp.getParentId());
                condition.setIsParent(false);
                condition.setUpdated(new Date());
                tbContentCategoryMapper.updateByPrimaryKeySelective(condition);
            }

            // 设置响应状态、信息
            if (rows > 0) {
                return TaotaoResult.ok();
            }
            return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "删除失败!");
        }

        // 重命名类目
        category.setName(name);
        int rows = tbContentCategoryMapper.updateByPrimaryKeySelective(category);
        if (rows > 0) {
            return TaotaoResult.ok();
        }
        return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "重命名失败！");
    }

    @Override
    public TaotaoResult updateCategory(Long id) {
        return updateCategory(id, null);
    }

    /**
     * 查询类目是否是父类目
     *
     * @param id 类目id
     * @return
     */
    private boolean isParent(Long id) {
        TbContentCategory category = tbContentCategoryMapper.selectByPrimaryKey(id);
        return category.getIsParent();
    }
}
