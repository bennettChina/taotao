package com.taotao.manager.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.taotao.common.pojo.EasyUiResult;
import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.HttpClientUtil;
import com.taotao.manager.mapper.TbContentMapper;
import com.taotao.manager.pojo.TbContent;
import com.taotao.manager.pojo.TbContentExample;
import com.taotao.manager.service.TbContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 内容管理相关业务逻辑实现类
 *
 * @author 召
 */

@PropertySource("classpath:/rest_ful.properties")
@Service
public class TbContentServiceImpl implements TbContentService {

    @Autowired
    private TbContentMapper tbContentMapper;

    @Value("${taotao.rest.host}")
    private String host;
    @Value("${taotao.rest.url.itemContent}")
    private String url;

    @Transactional(readOnly = true)
    @Override
    public EasyUiResult getContentList(Long categoryId, int pageNum, int pageSize) {
        // 设置分页属性
        PageHelper.startPage(pageNum, pageSize);

        // 创建查询条件
        TbContentExample condition = new TbContentExample();
        condition.createCriteria().andCategoryIdEqualTo(categoryId);

        // 执行分页查询
        Page<TbContent> tbContentPage = (Page<TbContent>) tbContentMapper.selectByExampleWithBLOBs(condition);

        long total = tbContentPage.getTotal();
        return new EasyUiResult(total, tbContentPage.getResult());
    }

    @Transactional(rollbackFor = java.lang.Exception.class)
    @Override
    public TaotaoResult saveContentList(TbContent tbContent) {
        // 补充新增的内容的字段
        Date date = new Date();
        tbContent.setCreated(date);
        tbContent.setUpdated(date);
        int rows = tbContentMapper.insert(tbContent);

        // 设置响应信息、状态码
        if (rows > 0) {
            // 清理缓存
            HttpClientUtil.doGet(host + url + tbContent.getCategoryId());
            return TaotaoResult.ok();
        }
        return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "添加失败！");
    }

    @Transactional(rollbackFor = java.lang.Exception.class)
    @Override
    public TaotaoResult editContentList(TbContent tbContent) {
        if (tbContent != null && tbContent.getId() != null) {
            tbContent.setUpdated(new Date());
            tbContentMapper.updateByPrimaryKeySelective(tbContent);
            // 清理缓存
            HttpClientUtil.doGet(host + url + tbContent.getCategoryId());
            return TaotaoResult.ok();
        }
        return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "操作非法！");
    }

    @Transactional(rollbackFor = java.lang.Exception.class)
    @Override
    public TaotaoResult deleteContentList(Long[] ids) {
        TbContent tbContent;
        // 判断删除操作
        if (ids != null && ids.length > 0) {
            // 查出该次操作的categoryId
            tbContent = tbContentMapper.selectByPrimaryKey(ids[0]);
            long rows = 0;
            for (Long id : ids) {
                rows += tbContentMapper.deleteByPrimaryKey(id);
            }
            if (rows > 0) {
                // 清理缓存
                HttpClientUtil.doGet(host + url + tbContent.getCategoryId());
                return TaotaoResult.ok();
            }
            return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "删除失败！");
        }
        return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "操作非法！");
    }
}
