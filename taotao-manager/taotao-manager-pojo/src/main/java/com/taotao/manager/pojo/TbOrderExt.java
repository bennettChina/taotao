package com.taotao.manager.pojo;

import java.util.List;

/**
 * 返回结果的扩展类
 *
 * @author 召
 */

public class TbOrderExt extends TbOrder {

    /**
     * 订单的商品列表
     */
    private List<TbOrderItem> orderItems;

    /**
     * 物流信息
     */
    private TbOrderShipping orderShipping;

    public List<TbOrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<TbOrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public TbOrderShipping getOrderShipping() {
        return orderShipping;
    }

    public void setOrderShipping(TbOrderShipping orderShipping) {
        this.orderShipping = orderShipping;
    }
}
