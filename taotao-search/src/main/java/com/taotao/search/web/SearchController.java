package com.taotao.search.web;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.search.pojo.SearchResult;
import com.taotao.search.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 搜索模块处理搜索相关请求
 *
 * @author 召
 */

@RestController
public class SearchController {

    @Autowired
    private SearchService searchService;

    @RequestMapping("/search")
    public TaotaoResult search(@RequestParam("q") String keywords, @RequestParam(defaultValue = "1") Integer pageNum,
                               @RequestParam(defaultValue = "24") Integer pageSize) {
        SearchResult searchResult = searchService.search(keywords, pageNum, pageSize);
        if (searchResult == null) {
            return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "无该商品的数据");
        }
        return TaotaoResult.ok(searchResult);
    }

    @CrossOrigin
    @RequestMapping("/import_item")
    public TaotaoResult importItemToIndex() {
        return searchService.importItemToIndex();
    }
}
