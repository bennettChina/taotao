package com.taotao.search.service;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.search.pojo.SearchResult;

/**
 * search模块中的搜索业务逻辑接口
 *
 * @author 召
 */

public interface SearchService {

    /**
     * 搜索商品
     *
     * @param keywords 关键字
     * @param pageNum  分页：当前页码
     * @param pageSize 分页：每页数据量
     * @return
     */
    SearchResult search(String keywords, Integer pageNum, Integer pageSize);

    /**
     * 导入数据库中的商品信息到solr中
     *
     * @return
     */
    TaotaoResult importItemToIndex();
}
