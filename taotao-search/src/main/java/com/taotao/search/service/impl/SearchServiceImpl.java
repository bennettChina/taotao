package com.taotao.search.service.impl;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.ExceptionUtil;
import com.taotao.manager.mapper.SearchItemMapper;
import com.taotao.manager.pojo.SearchItem;
import com.taotao.search.pojo.SearchResult;
import com.taotao.search.service.SearchService;
import lombok.extern.slf4j.Slf4j;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.CommonParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * search模块中的搜索业务逻辑实现类
 *
 * @author 召
 */

@Slf4j
@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    private SearchItemMapper searchItemMapper;
    @Autowired
    private SolrClient solrClient;

    @Value("${spring.data.solr.highLightField}")
    private String highLightField;
    @Value("${spring.data.solr.batch-size}")
    private Integer batchSize;

    @Override
    public SearchResult search(String keywords, Integer pageNum, Integer pageSize) {
        SolrQuery query = new SolrQuery();

        // 设置查询关键字
        query.setQuery(keywords);
        query.set(CommonParams.DF, "item_keywords");

        // 设置搜索分页参数
        query.setStart((pageNum - 1) * pageSize);
        query.setRows(pageSize);

        // 设置关键字高亮显示
        query.setHighlight(true);
        // 添加支持高亮的字段
        query.addHighlightField(highLightField);
        query.setHighlightSimplePre("<em style='color:red;'>");
        query.setHighlightSimplePost("</em>");

        // 获取搜索结果
        try {
            QueryResponse queryResponse = solrClient.query(query);
            SolrDocumentList resultList = queryResponse.getResults();
            /*
             * Solr返回的高亮信息格式：
             *   {
             *      "1202453093":{
             *          "item_title":[
             *              "唐为 TW509 移动联通2G<em>手机</em> 翻盖<em>手机</em> 双卡双待 老人<em>手机</em> 金色"
             *          ]
             *       }
             *   }
             * 获取Solr关键词高亮外层
             *
             * Map： key -> id  value: Map
             * 内层Map: key -> 咱们设置的高亮字段名 value: List
             * 最内层的List: 每个元素是一个已经携带了高亮信息的高亮字段内容（）
             **/
            Map<String, Map<String, List<String>>> highlighting = queryResponse.getHighlighting();
            SearchResult searchResult = new SearchResult();
            // 结果封装
            List<SearchItem> searchItemList = parseSearchDocument(resultList, highlighting, highLightField);
            searchResult.setItemList(searchItemList);

            // 获取搜索结果的总记录数
            long totalCount = resultList.getNumFound();
            // 计算总页数
            long totalPages = totalCount / pageSize;
            if (totalCount % pageSize != 0) {
                totalPages++;
            }

            // 设置返回结果的属性
            searchResult.setPageNum(pageNum);
            searchResult.setTotalPages((int) totalPages);

            // 返回结果
            return searchResult;
        } catch (SolrServerException e) {
            log.error("查询失败！异常栈信息：" + ExceptionUtil.getStackTrace(e));
        } catch (IOException e) {
            log.error("查询失败！异常栈信息：" + ExceptionUtil.getStackTrace(e));
        }
        return null;
    }

    @Transactional(readOnly = true)
    @Override
    public TaotaoResult importItemToIndex() {
        //查询商品列表
        List<SearchItem> itemList = searchItemMapper.selectAllItem();

        // 定义批量操作暂存队列
        long countResult = 0;
        List<SolrInputDocument> batchList = new ArrayList<>();
        try {
            for (int i = 0; i < itemList.size(); i++) {
                //将对象转换为SolrInputDocument对象
                SearchItem searchItem = itemList.get(i);
                SolrInputDocument doc = new SolrInputDocument();
                // 设置各字段的值
                doc.addField("id", searchItem.getId());
                doc.addField("item_title", searchItem.getTitle());
                doc.addField("item_price", searchItem.getPrice());
                doc.addField("item_image", searchItem.getImage());
                doc.addField("item_sell_point", searchItem.getSellPoint());
                doc.addField("item_category_name", searchItem.getCategoryName());
                doc.addField("item_desc", searchItem.getDesc());
                // 将创建好的待存的SolrInputDocument对象添加到批量暂存队列
                batchList.add(doc);
                // 批量添加到solrClient
                if (batchList.size() % batchSize == 0) {
                    System.out.println("批量导入solr500条");
                    solrClient.add(batchList);
                    countResult += batchSize;
                    batchList.clear();
                }
            }
            // 添加剩下的数据
            if (batchList.size() > 0) {
                System.out.println("批量导入solr" + batchList.size() + "条");
                solrClient.add(batchList);
                countResult += batchList.size();
                batchList.clear();
            }
            // 提交到Solr服务器
            solrClient.commit();
        } catch (SolrServerException e) {
            log.error("导入失败！异常栈信息：" + ExceptionUtil.getStackTrace(e));
        } catch (IOException e) {
            log.error("导入失败！异常栈信息：" + ExceptionUtil.getStackTrace(e));
        }

        if (countResult > 0) {
            return TaotaoResult.ok();
        }
        return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_ERROR, "添加失败！");
    }

    /**
     * 将Solr结果封装成我们自己的SearchItem对象
     *
     * @param resultList     结果集
     * @param highlighting   高亮数据
     * @param highLightField 高亮字段
     * @return
     */
    private List<SearchItem> parseSearchDocument(SolrDocumentList resultList, Map<String, Map<String, List<String>>> highlighting, String highLightField) {
        List<SearchItem> result = new ArrayList<>();
        Iterator<SolrDocument> iterator = resultList.iterator();
        while (iterator.hasNext()) {
            SolrDocument document = iterator.next();

            // 获取solr结果集中的各字段的值
            String title = (String) document.getFieldValue("item_title");
            String image = (String) document.getFieldValue("item_image");
            String sid = (String) document.getFieldValue("id");
            Long id = Long.valueOf(sid);
            Long price = (Long) document.getFieldValue("item_price");
            String categoryName = (String) document.getFieldValue("item_category_name");
            String sellPoint = (String) document.getFieldValue("item_sell_point");
            String desc = (String) document.getFieldValue("item_desc");

            // 处理solr字段高亮
            if (highlighting != null && highlighting.size() > 0) {
                if (highlighting.containsKey(sid)) {
                    Map<String, List<String>> highLightMap = highlighting.get(sid);
                    if (highLightMap != null && highLightMap.size() > 0) {
                        title = highLightMap.get(highLightField).get(0);
                    }
                }
            }

            // 设置字段的值到SearchItem中
            SearchItem searchItem = new SearchItem();
            searchItem.setId(id);
            searchItem.setTitle(title);
            searchItem.setPrice(price);
            searchItem.setImage(image);
            searchItem.setSellPoint(sellPoint);
            searchItem.setCategoryName(categoryName);
            searchItem.setDesc(desc);

            // 将数据加到结果中
            result.add(searchItem);
        }
        return result;
    }
}
