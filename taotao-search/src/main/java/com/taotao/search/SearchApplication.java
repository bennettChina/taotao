package com.taotao.search;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
*
*@author 召
*/

@SpringBootApplication
@MapperScan("com.taotao.manager.mapper")
public class SearchApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(SearchApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(SearchApplication.class, args);
    }
}
