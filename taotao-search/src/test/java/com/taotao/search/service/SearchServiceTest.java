package com.taotao.search.service;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.JsonUtils;
import com.taotao.search.pojo.SearchResult;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author 召
 */

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class SearchServiceTest {

    @Autowired
    private SearchService searchService;

    @Test
    public void testInsertDataToTaoTao() {
        TaotaoResult result = searchService.importItemToIndex();
        int status = result.getStatus();
        Assert.assertEquals("导入失败！", TaotaoResult.RESPONSE_CODE_OK, status);
    }

    @Test
    public void testSearch() {
        SearchResult result = searchService.search("手机", 1, 10);
        System.out.println(JsonUtils.objectToJson(result));
        Assert.assertNotNull("无数据或查询失败！", result.getItemList());
    }
}
