package com.taotao.portal.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.taotao.common.utils.StringUtils;
import com.taotao.manager.pojo.TbItem;

/**
 * 由于商品图片可能有多张， 为了满足前端页面展示需要，
 * 我们给TbItem扩展一个方法来处理多张图片的情况
 *
 * @author 召
 */

public class TbItemExt extends TbItem {

    /**
     * 将数据库中image字段存储的“,”分隔格式的多张图片，转化成一个String数组
     * 序列化为json时忽略此方法
     * @return
     */
    @JsonIgnore
    public String[] getImages() {
        // 以“，”号分割字符串
        if (StringUtils.isBlank(this.getImage())) {
            return this.getImage().split(",");
        }
        return null;
    }
}
