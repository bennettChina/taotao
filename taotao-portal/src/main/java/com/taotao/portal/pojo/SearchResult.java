package com.taotao.portal.pojo;


import com.taotao.manager.pojo.SearchItem;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * 搜索结果的实体类
 *
 * @author 召
 */

@Setter
@Getter
public class SearchResult implements Serializable {

    private Integer pageNum;
    private Integer totalPages;
    private List<SearchItem> itemList;
}
