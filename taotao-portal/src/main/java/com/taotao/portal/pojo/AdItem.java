package com.taotao.portal.pojo;

import lombok.Getter;
import lombok.Setter;

/**
 * 广告位数据结构实体类
 *
 * @author 召
 */

@Setter
@Getter
// @PropertySource("classpath:/resource.properties")
// @EnableConfigurationProperties(AdItem.class)
// @ConfigurationProperties(prefix = "ad")
public class AdItem {

    /**
     * 小广告位的高
     */
    private Integer height;

    /**
     * 小广告位的宽
     */
    private Integer width;

    /**
     * 小广告位的图片url
     */
    private String src;

    /**
     * 大广告位的高
     */
    private Integer heightB;

    /**
     * 大广告位的宽
     */
    private Integer widthB;

    /**
     * 大广告位的图片url
     */
    private String srcB;

    /**
     * 图片的描述信息
     */
    private String alt;

    /**
     * 请求的url
     */
    private String href;
}
