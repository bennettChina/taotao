package com.taotao.portal.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author 召
 */

@Controller
public class PageController {

    /**
     * 跳转到对应的页面
     *
     * @param page 页面
     * @return
     */
    @RequestMapping("/{page}")
    public String toPage(@PathVariable("page") String page) {
        return page;
    }
}
