package com.taotao.portal.web;

import com.taotao.portal.pojo.TbItemExt;
import com.taotao.portal.service.TbItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author 召
 */

@Controller
@RequestMapping("/item")
public class TbItemController {

    @Autowired
    private TbItemService tbItemService;

    @RequestMapping("/{item_id}")
    public String getItemDetails(@PathVariable("item_id") Long itemId, Model model) {
        TbItemExt tbItemExt = tbItemService.getItemDetails(itemId);
        model.addAttribute("item", tbItemExt);
        return "item";
    }

    @ResponseBody
    @RequestMapping("/desc/{item_id}")
    public String getItemDesc(@PathVariable("item_id") Long itemId) {
        return tbItemService.getItemDesc(itemId);
    }

    @ResponseBody
    @RequestMapping("/param/{item_id}")
    public String getItemParam(@PathVariable("item_id") Long itemId) {
        return tbItemService.getItemParam(itemId);
    }
}
