package com.taotao.portal.web;

import com.taotao.portal.service.AdService;
import com.taotao.portal.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 门户网站页面跳转
 *
 * @author 召
 */

@Controller
public class IndexController {

    @Autowired
    private AdService adService;

    @Autowired
    private UserService userService;

    /**
     * 获取广告位的内容
     *
     * @param model
     * @return
     */
    @RequestMapping({"/", "/index"})
    public String showIndex(Model model) {
        String adResult = adService.getBigAdItemList();
        model.addAttribute("ad1", adResult);
        return "index";
    }

    @RequestMapping("/user/logout")
    public String logout(HttpServletRequest request) {
        userService.logout(request);
        return "redirect:/";
    }
}
