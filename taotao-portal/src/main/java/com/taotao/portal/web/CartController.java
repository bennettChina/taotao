package com.taotao.portal.web;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.portal.pojo.TbItemExt;
import com.taotao.portal.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 处理购物车相关功能
 *
 * @author 召
 */

@Controller
@RequestMapping("/cart")
public class CartController {

    @Autowired
    private CartService cartService;

    /**
     * 添加商品到购物车
     *
     * @param itemId   商品id
     * @param request
     * @param response
     * @param model
     * @return
     */
    @RequestMapping("/add/{itemId}")
    public String addItem(@PathVariable Long itemId, HttpServletRequest request,
                          HttpServletResponse response, Model model) {
        // 添加商品信息
        TaotaoResult taotaoResult = cartService.addItem(itemId, request, response);
        // 返回结果
        if (taotaoResult != null && taotaoResult.getStatus().equals(TaotaoResult.RESPONSE_CODE_OK)) {
            model.addAttribute("cartList", taotaoResult.getData());
            return "redirect:/cart/cart.html";
        }
        model.addAttribute("message", taotaoResult.getMsg());
        return "error/exception";
    }

    /**
     * 展示商品列表
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping({"/cart", "/show"})
    public String showCart(HttpServletRequest request, Model model) {
        // 取购物车信息
        List<TbItemExt> itemExtList = cartService.getCartItemList(request);
        model.addAttribute("cartList", itemExtList);
        return "cart";
    }

    /**
     * 修改购物车中的商品数量
     *
     * @param itemId   商品id
     * @param num      商品数量
     * @param request
     * @param response
     * @return
     */
    @ResponseBody
    @RequestMapping("/update/num/{itemId}/{num}")
    public TaotaoResult updateItemNumById(@PathVariable Long itemId, @PathVariable Integer num,
                                          HttpServletRequest request, HttpServletResponse response) {
        return cartService.modifyItemNum(itemId, num, request, response);
    }

    @RequestMapping("/delete/{itemId}")
    public String deleteItemById(@PathVariable Long itemId, HttpServletRequest request,
                                 HttpServletResponse response, Model model) {
        List<TbItemExt> itemExtList = cartService.deleteItemById(itemId, request, response);
        model.addAttribute("cartList", itemExtList);
        return "cart";
    }
}
