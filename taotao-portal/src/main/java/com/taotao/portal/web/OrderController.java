package com.taotao.portal.web;

import com.taotao.manager.pojo.TbOrderExt;
import com.taotao.portal.pojo.OrderResult;
import com.taotao.portal.pojo.TbItemExt;
import com.taotao.portal.service.CartService;
import com.taotao.portal.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author 召
 */

@Slf4j
@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;
    @Autowired
    private CartService cartService;

    /**
     * 展示用户订单购物车
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping("/order-cart")
    public String showOrderCart(HttpServletRequest request, Model model) {
        //根据用户信息，取出用户的收货地址列表
        //本项目中使用静态数据模拟。。。。

        //从cookie中把商品列表取出来
        List<TbItemExt> itemsList = cartService.getCartItemList(request);
        model.addAttribute("cartList", itemsList);
        return "order-cart";
    }

    /**
     * 创建订单
     *
     * @param orderExt 订单信息
     * @param model
     * @return
     */
    @PostMapping("/create")
    public String createOrder(TbOrderExt orderExt, HttpServletRequest request,
                              HttpServletResponse response, Model model) {
        OrderResult result = orderService.createOrder(orderExt, request, response);
        try {
            model.addAttribute("orderId", result.getOrderId());
            model.addAttribute("payment", result.getTotalPrice());
            model.addAttribute("date", result.getDate());
            return "success";
        } catch (Exception e) {
            log.error("创建订单失败", e);
            return "error/exception";
        }
    }
}
