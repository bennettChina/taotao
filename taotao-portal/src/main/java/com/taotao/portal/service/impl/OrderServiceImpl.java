package com.taotao.portal.service.impl;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.CookieUtils;
import com.taotao.common.utils.HttpClientUtil;
import com.taotao.common.utils.JsonUtils;
import com.taotao.manager.pojo.TbOrderExt;
import com.taotao.portal.pojo.OrderResult;
import com.taotao.portal.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * 门户网址的订单业务处理实现类
 *
 * @author 召
 */

@Slf4j
@Service
@PropertySource("classpath:/resource.properties")
public class OrderServiceImpl implements OrderService {

    @Value("${taotao.order.baseUrl}")
    private String orderBaseUrl;
    @Value("${taotao.order.createOrder}")
    private String createOrderUrl;
    @Value("${taotao.sso.login.token.name}")
    private String loginTokenName;
    /**
     * COOKIE中购物车商品对应的key
     */
    @Value("${taotao.cookie.cart.itemListKey}")
    private String cartItemListKey;

    /**
     * 创建订单
     *
     * @param orderExt 订单信息
     * @param request
     * @return
     */
    @Override
    public OrderResult createOrder(TbOrderExt orderExt, HttpServletRequest request, HttpServletResponse response) {
        String token = CookieUtils.getCookieValue(request, loginTokenName);
        // 创建请求参数
        Map<String, String> params = new HashMap<>(16);
        params.put("data", JsonUtils.objectToJson(orderExt));
        params.put("token", token);

        try {
            // 发起后端请求
            String json = HttpClientUtil.doPost(orderBaseUrl + createOrderUrl, params);
            TaotaoResult taotaoResult = TaotaoResult.formatToPojo(json, OrderResult.class);
            if (taotaoResult != null && taotaoResult.getStatus().equals(TaotaoResult.RESPONSE_CODE_OK)) {
                // 成功后清空购物车
                CookieUtils.deleteCookie(request, response, cartItemListKey);
                return (OrderResult) taotaoResult.getData();
            }
        } catch (Exception e) {
            log.error("创建订单失败！", e);
        }
        return null;
    }
}
