package com.taotao.portal.service;

import com.taotao.portal.pojo.TbItemExt;

/**
 * 商品相关业务处理接口
 *
 * @author 召
 */

public interface TbItemService {

    /**
     * 获取商品的详细信息
     *
     * @param itemId 商品id
     * @return
     */
    TbItemExt getItemDetails(Long itemId);

    /**
     * 获取商品的描述信息
     *
     * @param itemId 商品id
     * @return
     */
    String getItemDesc(Long itemId);

    /**
     * 获取商品的规格参数
     *
     * @param itemId 商品id
     * @return
     */
    String getItemParam(Long itemId);
}
