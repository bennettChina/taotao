package com.taotao.portal.service;

import com.taotao.portal.pojo.SearchResult;

/**
 * 搜索相关业务逻辑接口
 *
 * @author 召
 */

public interface SearchService {

    /**
     * 商品搜索处理
     *
     * @param keywords 关键字
     * @param pageNum  当前页码
     * @param pageSize 每页数据量
     * @return
     */
    SearchResult search(String keywords, Integer pageNum, Integer pageSize);
}
