package com.taotao.portal.service;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 召
 */

public interface UserService {

    /**
     * 安全退出系统
     *
     * @param request
     */
    void logout(HttpServletRequest request);
}
