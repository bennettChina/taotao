package com.taotao.portal.service;

import com.taotao.manager.pojo.TbOrderExt;
import com.taotao.portal.pojo.OrderResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 门户网址的订单业务处理接口
 *
 * @author 召
 */

public interface OrderService {

    /**
     * 创建订单
     *
     * @param orderExt 订单信息
     * @param request
     * @return
     */
    OrderResult createOrder(TbOrderExt orderExt, HttpServletRequest request, HttpServletResponse response);
}
