package com.taotao.portal.service;

/**
 * 网站广告位业务逻辑接口
 *
 * @author 召
 */

public interface AdService {

    /**
     * 获取广告数据集合
     *
     * @return
     */
    String getBigAdItemList();
}
