package com.taotao.portal.service.impl;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.HttpClientUtil;
import com.taotao.common.utils.JsonUtils;
import com.taotao.manager.pojo.TbContent;
import com.taotao.portal.pojo.AdItem;
import com.taotao.portal.service.AdService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 网站广告位业务逻辑实现类
 *
 * @author 召
 */

@Service
@PropertySource("classpath:/resource.properties")
public class AdServiceImpl implements AdService {

    @Value("${ad.width}")
    private int width;
    @Value("${ad.height}")
    private int height;
    @Value("${ad.widthB}")
    private int widthB;
    @Value("${ad.heightB}")
    private int heightB;
    @Value("${rest.baseUrl}")
    private String restBaseUrl;
    @Value("${index.ad1.url}")
    private String indexAd1Url;
    @Value("${index.bigAdCategoryId}")
    private Long bigAdCategoryId;

    @Override
    public String getBigAdItemList() {
        return getContent(bigAdCategoryId);
    }

    private String getContent(Long categoryId) {
        // 调用服务层的服务查询广告位的数据
        String result = HttpClientUtil.doGet(restBaseUrl + indexAd1Url + categoryId);

        // 把json数据转为对象
        TaotaoResult taotaoResult = TaotaoResult.formatToList(result, TbContent.class);
        List<AdItem> adItemList = new ArrayList<>();
        // 正常响应后将拿到的数据设置到对应的属性中
        boolean isOk = taotaoResult != null && taotaoResult.getStatus() == TaotaoResult.RESPONSE_CODE_OK;
        if (isOk) {
            List<TbContent> tbContentList = (List<TbContent>) taotaoResult.getData();
            for (TbContent tbContent : tbContentList) {
                AdItem item = new AdItem();
                // 其他属性通过配置文件，自动注入(失败了下面手动注入)
                item.setWidth(width);
                item.setHeight(height);
                item.setWidthB(widthB);
                item.setHeightB(heightB);
                item.setAlt(tbContent.getTitleDesc());
                item.setSrc(tbContent.getPic());
                item.setSrcB(tbContent.getPic2());
                adItemList.add(item);
            }
        }
        return JsonUtils.objectToJson(adItemList);
    }
}
