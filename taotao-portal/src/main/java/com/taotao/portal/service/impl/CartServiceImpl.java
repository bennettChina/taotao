package com.taotao.portal.service.impl;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.CookieUtils;
import com.taotao.common.utils.HttpClientUtil;
import com.taotao.common.utils.JsonUtils;
import com.taotao.common.utils.StringUtils;
import com.taotao.manager.pojo.TbItem;
import com.taotao.portal.pojo.TbItemExt;
import com.taotao.portal.service.CartService;
import com.taotao.portal.util.TbObjectFormatUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author 召
 */

@Service
@PropertySource("classpath:/resource.properties")
public class CartServiceImpl implements CartService {

    /**
     * rest服务URL
     */
    @Value("${rest.baseUrl}")
    private String restBaseUrl;

    /**
     * 商品服务URL
     */
    @Value("${item.details.url}")
    private String itemUrl;

    /**
     * COOKIE中购物车商品对应的key
     */
    @Value("${taotao.cookie.cart.itemListKey}")
    private String cartItemListKey;

    /**
     * 购物车cookie生存期
     */
    @Value("${taotao.cookie.cart.expireTime.itemList}")
    private Long cartItemListExpireTime;

    /**
     * 添加购物车商品
     *
     * @param itemId   商品id
     * @param request
     * @param response
     * @return
     */
    @Override
    public TaotaoResult addItem(Long itemId, HttpServletRequest request, HttpServletResponse response) {
        // 根据商品id获取商品信息
        TbItemExt tbItemExt = getItemById(itemId);
        if (tbItemExt == null) {
            return TaotaoResult.build(TaotaoResult.RESPONSE_CODE_BAD_REQUEST, "没有该商品的信息");
        }

        // 取cookie中的商品列表
        List<TbItemExt> itemExtList = getItemListFromCookie(request);

        // 判断购物车中是否已有该商品
        boolean isExists = false;
        for (TbItemExt itemExt : itemExtList) {
            if (itemId.equals(itemExt.getId())) {
                // 购物车中已有该商品，商品数量加一
                itemExt.setNum(itemExt.getNum() + 1);
                isExists = true;
                break;
            }
        }

        // 购物车中没有该商品，则添加一个到购物车中
        if (!isExists) {
            // 设置数量为1
            tbItemExt.setNum(1);
            // 把商品添加到购物车中
            itemExtList.add(tbItemExt);
        }

        // 把购物车信息写入cookie，返回结果
        saveCartItemList2Cookie(itemExtList, request, response);
        return TaotaoResult.ok(itemExtList);
    }

    /**
     * 获取商品列表
     *
     * @param request
     * @return
     */
    @Override
    public List<TbItemExt> getCartItemList(HttpServletRequest request) {
        return getItemListFromCookie(request);
    }

    /**
     * 修改商品数量
     *
     * @param itemId   商品id
     * @param num      商品数量
     * @param request
     * @param response
     * @return
     */
    @Override
    public TaotaoResult modifyItemNum(Long itemId, Integer num, HttpServletRequest request, HttpServletResponse response) {
        // 从cookie中获取商品列表
        List<TbItemExt> itemExtList = getItemListFromCookie(request);
        // 从商品列表中找到要修改的商品
        for (TbItemExt itemExt : itemExtList) {
            if (itemId.equals(itemExt.getId())) {
                // 修改数量
                itemExt.setNum(num);
                break;
            }
        }

        // 把商品列表重新写入cookie
        saveCartItemList2Cookie(itemExtList, request, response);
        return TaotaoResult.ok();
    }

    /**
     * 删除购物车中的商品
     *
     * @param itemId   商品id
     * @param request
     * @param response
     * @return
     */
    @Override
    public List<TbItemExt> deleteItemById(Long itemId, HttpServletRequest request, HttpServletResponse response) {
        // 获取cookie中的商品列表
        List<TbItemExt> itemExtList = getItemListFromCookie(request);
        // 找到要删除的商品，删除它
        for (TbItemExt itemExt : itemExtList) {
            if (itemId.equals(itemExt.getId())) {
                itemExtList.remove(itemExt);
                break;
            }
        }

        // 重新写入cookie
        saveCartItemList2Cookie(itemExtList, request, response);
        return itemExtList;
    }

    /**
     * 取cookie中的商品列表
     *
     * @param request
     * @return
     */
    private List<TbItemExt> getItemListFromCookie(HttpServletRequest request) {
        // 取cookie中的商品列表
        String cartItemListJson = CookieUtils.getCookieValue(request, cartItemListKey, true);
        // 如果不空则可以转为Java对象
        if (!StringUtils.isBlank(cartItemListJson)) {
            return JsonUtils.jsonToList(cartItemListJson, TbItemExt.class);
        }
        return new ArrayList<>();
    }

    /**
     * 获取商品信息
     *
     * @param itemId 商品id
     * @return
     */
    private TbItemExt getItemById(Long itemId) {
        // 根据商品id查询商品信息
        String itemJson = HttpClientUtil.doGet(restBaseUrl + itemUrl + itemId);
        TaotaoResult taotaoResult = TaotaoResult.formatToPojo(itemJson, TbItem.class);
        // 返回商品信息
        if (taotaoResult != null && taotaoResult.getStatus().equals(TaotaoResult.RESPONSE_CODE_OK)) {
            TbItem tbItem = (TbItem) taotaoResult.getData();
            return TbObjectFormatUtils.parseTbItem2TbItemExt(tbItem, new TbItemExt());
        }
        return null;
    }

    /**
     * 把购物车信息写入cookie
     *
     * @param itemExtList 购物车商品列表
     * @param request
     * @param response
     */
    private void saveCartItemList2Cookie(List<TbItemExt> itemExtList, HttpServletRequest request, HttpServletResponse response) {
        int expireTime = (int) TimeUnit.MINUTES.toSeconds(cartItemListExpireTime);
        String items = JsonUtils.objectToJson(itemExtList);
        CookieUtils.setCookie(request, response, cartItemListKey, items, expireTime, true);
    }
}
