package com.taotao.portal.service.impl;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.CookieUtils;
import com.taotao.common.utils.HttpClientUtil;
import com.taotao.portal.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 召
 */

@Slf4j
@Service
@PropertySource("classpath:/resource.properties")
public class UserServiceImpl implements UserService {

    @Value("${taotao.sso.login.token.name}")
    private String ssoLoginToken;
    @Value("${taotao.sso.baseUrl}")
    private String ssoBaseUrl;
    @Value("${taotao.sso.logout.url}")
    private String userLogoutUrl;

    /**
     * 安全退出系统
     *
     * @param request
     */
    @Override
    public void logout(HttpServletRequest request) {
        String token = CookieUtils.getCookieValue(request, ssoLoginToken);
        String json = HttpClientUtil.doGet(ssoBaseUrl + userLogoutUrl + token);
        try {
            TaotaoResult taotaoResult = TaotaoResult.format(json);
            if (taotaoResult != null && taotaoResult.getStatus().equals(TaotaoResult.RESPONSE_CODE_OK)) {
                log.info("用户已安全退出！");
            }
        } catch (Exception e) {
            log.error("安全退出异常!", e);
        }
    }
}
