package com.taotao.portal.service.impl;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.HttpClientUtil;
import com.taotao.common.utils.JsonUtils;
import com.taotao.manager.pojo.TbItem;
import com.taotao.manager.pojo.TbItemDesc;
import com.taotao.manager.pojo.TbItemParamItem;
import com.taotao.portal.pojo.TbItemExt;
import com.taotao.portal.service.TbItemService;
import com.taotao.portal.util.TbObjectFormatUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author 召
 */

@Slf4j
@PropertySource("classpath:/resource.properties")
@Service
public class TbItemServiceImpl implements TbItemService {

    @Value("${rest.baseUrl}")
    private String baseRestUrl;
    @Value("${item.details.url}")
    private String itemDetailsUrl;
    @Value("${item.desc.url}")
    private String itemDescUrl;
    @Value("${item.param.url}")
    private String itemParamUrl;

    @Override
    public TbItemExt getItemDetails(Long itemId) {
        // 发起rest服务请求，获取数据
        String resultJson = HttpClientUtil.doGet(baseRestUrl + itemDetailsUrl + itemId);
        // json字符串转化
        TaotaoResult taotaoResult = TaotaoResult.formatToPojo(resultJson, TbItem.class);

        if (taotaoResult != null && taotaoResult.getStatus().equals(TaotaoResult.RESPONSE_CODE_OK)) {
            TbItem tbItem = (TbItem) taotaoResult.getData();
            return TbObjectFormatUtils.parseTbItem2TbItemExt(tbItem, new TbItemExt());
        }
        log.error("获取商品详情失败：" + taotaoResult.getMsg() + "\n异常栈信息：" + taotaoResult.getData());
        return null;
    }

    @Override
    public String getItemDesc(Long itemId) {
        // 发起rest服务请求，获取数据
        String resultJson = HttpClientUtil.doGet(baseRestUrl + itemDescUrl + itemId);
        // json字符串转化
        TaotaoResult taotaoResult = TaotaoResult.formatToPojo(resultJson, TbItemDesc.class);

        if (taotaoResult != null && taotaoResult.getStatus().equals(TaotaoResult.RESPONSE_CODE_OK)) {
            TbItemDesc tbItemDesc = (TbItemDesc) taotaoResult.getData();
            return tbItemDesc.getItemDesc();
        }
        log.error("请求商品描述信息出错：" + taotaoResult.getMsg() + "\n异常栈信息：" + taotaoResult.getData());
        return "<span>暂无描述</span>";
    }

    @Override
    public String getItemParam(Long itemId) {
        String jsonResult = HttpClientUtil.doGet(baseRestUrl + itemParamUrl + itemId);
        TaotaoResult taotaoResult = TaotaoResult.formatToPojo(jsonResult, TbItemParamItem.class);
        if (taotaoResult != null && taotaoResult.getStatus().equals(TaotaoResult.RESPONSE_CODE_OK)) {
            TbItemParamItem itemParam = (TbItemParamItem) taotaoResult.getData();
            return buildHtmlFromTbItemParamItem(itemParam);
        }
        log.error("获取商品规格参数失败: " + taotaoResult.getMsg() + "\n异常栈信息：" + taotaoResult.getData());
        return "<span>暂无商品规格参数<span>";
    }

    /**
     * 将获取到的规格拼成一个HTML网页
     *
     * @param itemParamItem 商品的规格参数
     * @return
     */
    private String buildHtmlFromTbItemParamItem(TbItemParamItem itemParamItem) {
        StringBuilder sb = new StringBuilder();
        // 在下面拼出一个响应到页面的HTML网页
        sb.append("<table cellpadding=\"0\" cellspacing=\"1\" width=\"100%\" border=\"0\" class=\"Ptable\">");
        sb.append("<tbody>");
        // 把数据库中存储的商品规格参数字符串转换成Java对象，方便下面组装html
        List<Map> paramDataItemList = JsonUtils.jsonToList(itemParamItem.getParamData(), Map.class);
        // 把数据库中存储的商品规格参数字符串转换成Java对象，方便下面组装html
        if (paramDataItemList != null) {
            for (Map itemParamMap : paramDataItemList) {
                String groupName = (String) itemParamMap.get("group");
                sb.append("<tr>");
                sb.append("<th class=\"tdTitle\" colspan=\"2\">");
                sb.append(groupName);
                sb.append("</th>");
                sb.append("</tr>");
                // 第二层循环是拼接一个规格参数主体下面每一项规格参数的HTML
                List<Map> params = (List<Map>) itemParamMap.get("params");
                for (Map val : params) {
                    sb.append("<tr>");
                    sb.append("<td class=\"tdTitle\">");
                    sb.append(val.get("k"));
                    sb.append("</td>");
                    sb.append("<td>");
                    sb.append(val.get("v"));
                    sb.append("</td>");
                    sb.append("</tr>");
                }
            }
        }
        sb.append("</tbody>");
        sb.append("</table>");
        return sb.toString();
    }
}
