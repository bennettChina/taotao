package com.taotao.portal.service.impl;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.HttpClientUtil;
import com.taotao.portal.pojo.SearchResult;
import com.taotao.portal.service.SearchService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 搜索相关业务逻辑实现类
 *
 * @author 召
 */
@PropertySource("classpath:/resource.properties")
@Service
public class SearchServiceImpl implements SearchService {

    @Value("${taotao.search.baseUrl}")
    private String searchBaseUrl;
    @Value("${taotao.search.url}")
    private String searchUrl;

    @Override
    public SearchResult search(String keywords, Integer pageNum, Integer pageSize) {
        // 添加请求参数
        Map<String, String> params = new HashMap<>();
        params.put("q", keywords);
        params.put("pageNum", pageNum.toString());
        params.put("pageSize", pageSize.toString());

        // 发起请求
        String searchResultJson = HttpClientUtil.doPost(searchBaseUrl + searchUrl, params);

        // 获取返回的数据,并返回结果
        TaotaoResult taotaoResult = TaotaoResult.formatToPojo(searchResultJson, SearchResult.class);
        if (taotaoResult != null && taotaoResult.getStatus().equals(TaotaoResult.RESPONSE_CODE_OK)) {
            return (SearchResult) taotaoResult.getData();
        }
        return new SearchResult();
    }
}
