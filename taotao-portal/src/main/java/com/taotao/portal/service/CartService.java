package com.taotao.portal.service;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.portal.pojo.TbItemExt;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author 召
 */

public interface CartService {

    /**
     * 添加购物车商品
     *
     * @param itemId   商品id
     * @param request
     * @param response
     * @return
     */
    TaotaoResult addItem(Long itemId, HttpServletRequest request, HttpServletResponse response);

    /**
     * 获取商品列表
     *
     * @param request
     * @return
     */
    List<TbItemExt> getCartItemList(HttpServletRequest request);

    /**
     * 修改商品数量
     *
     * @param itemId   商品id
     * @param num      商品数量
     * @param request
     * @param response
     * @return
     */
    TaotaoResult modifyItemNum(Long itemId, Integer num, HttpServletRequest request, HttpServletResponse response);

    /**
     * 删除购物车中的商品
     *
     * @param itemId   商品id
     * @param request
     * @param response
     * @return
     */
    List<TbItemExt> deleteItemById(Long itemId, HttpServletRequest request, HttpServletResponse response);
}
