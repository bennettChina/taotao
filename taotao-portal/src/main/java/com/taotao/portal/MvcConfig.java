package com.taotao.portal;

import com.taotao.portal.interceptor.CharacterInterceptor;
import com.taotao.portal.interceptor.OrderInterceptor;
import org.apache.tomcat.util.http.LegacyCookieProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * mvc配置类
 *
 * @author 召
 */

@Configuration
public class MvcConfig implements WebMvcConfigurer {


    @Autowired
    private OrderInterceptor orderInterceptor;
    @Autowired
    private CharacterInterceptor characterInterceptor;
    /**
     * 添加拦截器
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(characterInterceptor);
        registry.addInterceptor(orderInterceptor).addPathPatterns("/order/**");
    }

    /**
     * 改变SpringMvc dispatcherServlet默认配置
     *
     * @param dispatcherServlet
     * @return
     */
    @Bean
    public ServletRegistrationBean servletRegistrationBean(DispatcherServlet dispatcherServlet) {
        ServletRegistrationBean srb = new ServletRegistrationBean(dispatcherServlet);
        // 除了默认的/，增加对*.html后缀请求的处理
        srb.addUrlMappings("*.html");
        srb.addUrlMappings("/");
        return srb;
    }

    /**
     * 解决cookie子域名非法的问题
     *
     * @return
     */
    @Bean
    public WebServerFactoryCustomizer<TomcatServletWebServerFactory> cookieProcessorCustomizer() {
        return (factory) -> factory.addContextCustomizers((context) -> context.setCookieProcessor(new LegacyCookieProcessor()));
    }

}
