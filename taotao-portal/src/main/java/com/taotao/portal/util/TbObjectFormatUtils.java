package com.taotao.portal.util;

import com.taotao.manager.pojo.TbItem;
import com.taotao.portal.pojo.TbItemExt;

/**
 * 转化一些实体类对象为另一个子扩展类对象的工具类
 *
 * @author 召
 */

public class TbObjectFormatUtils {

    /**
     * 将TbItem对象转换成前端页面需要的TbItemExt对象
     *
     * @param tbItem
     * @param tbItemExt
     * @return
     */
    public static TbItemExt parseTbItem2TbItemExt(TbItem tbItem, TbItemExt tbItemExt) {
        tbItemExt.setId(tbItem.getId());
        tbItemExt.setTitle(tbItem.getTitle());
        tbItemExt.setSellPoint(tbItem.getSellPoint());
        tbItemExt.setImage(tbItem.getImage());
        tbItemExt.setNum(tbItem.getNum());
        tbItemExt.setPrice(tbItem.getPrice());
        tbItemExt.setCid(tbItem.getCid());
        tbItemExt.setBarcode(tbItem.getBarcode());
        tbItemExt.setStatus(tbItem.getStatus());
        tbItemExt.setCreated(tbItem.getCreated());
        tbItemExt.setUpdated(tbItem.getUpdated());
        return tbItemExt;
    }
}
