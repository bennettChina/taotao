package com.taotao.portal.interceptor;

import com.taotao.common.pojo.TaotaoResult;
import com.taotao.common.utils.CookieUtils;
import com.taotao.common.utils.HttpClientUtil;
import com.taotao.common.utils.StringUtils;
import com.taotao.manager.pojo.TbUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 召
 */

@Slf4j
@Component
@PropertySource("classpath:/resource.properties")
public class OrderInterceptor implements HandlerInterceptor {

    @Value("${taotao.sso.login.url}")
    private String ssoLoginUrl;
    @Value("${taotao.sso.token.url}")
    private String ssoTokenUrl;
    @Value("${taotao.sso.login.token.name}")
    private String tokenCookieName;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 获取cookie中的token
        String token = CookieUtils.getCookieValue(request, tokenCookieName);

        // 获取用户原本想去的页面
        String redirectUrl = request.getRequestURL().toString();

        // 如果没有token，重定向到登录页，并附加原始url
        if (StringUtils.isBlank(token)) {
            response.sendRedirect(ssoLoginUrl + "?redirect=" + redirectUrl);
            return false;
        }

        //拿从cookie中获取的token请求sso服务，获取真正的用户信息
        String jsonData = HttpClientUtil.doGet(ssoTokenUrl + "/" + token);
        try {
            // 用户token正常，将token对应的用户信息保存到request作用域
            TaotaoResult taotaoResult = TaotaoResult.formatToPojo(jsonData, TbUser.class);
            if (taotaoResult.getStatus().equals(TaotaoResult.RESPONSE_CODE_OK)) {
                TbUser tbUser = (TbUser) taotaoResult.getData();
                request.setAttribute("user", tbUser);
                return true;
            }

            // 用户会话过期后的处理
            response.getWriter().println("<script>alert('登录信息失效，请重新登录！');window.location.href='" +
                    ssoLoginUrl + "?redirect= " + redirectUrl + "';</script>");
        } catch (IOException e) {
            log.error("无法获取用户信息", e);
        }
        return false;
    }
}
